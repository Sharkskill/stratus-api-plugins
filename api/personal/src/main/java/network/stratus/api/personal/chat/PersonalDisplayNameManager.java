/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.personal.chat;

import network.stratus.api.personal.StratusAPIPersonal;
import network.stratus.api.pgm.chat.MapAuthorManager;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import tc.oc.pgm.api.match.Match;

/**
 * Manages display names on Ranked servers. If the player has the host group,
 * their prefixes will be overwritten by the host prefix. Otherwise, their
 * default prefixes are applied.
 * 
 * @author Ian Ballingall
 *
 * @deprecated Use realm-specific prefixes with {@link MapAuthorManager}.
 *             Will be removed in API 8.
 */
@Deprecated
public class PersonalDisplayNameManager {

	private final String ownerPrefix;
	private final String managerPrefix;

	public PersonalDisplayNameManager(String ownerPrefix, String managerPrefix) {
		this.ownerPrefix = ChatColor.translateAlternateColorCodes('`', ownerPrefix);
		this.managerPrefix = ChatColor.translateAlternateColorCodes('`', managerPrefix);
	}

//	@Override
	public void setDisplayName(Player player) {
//		boolean isOwner = StratusAPIPersonal.get().getOwner().equals(player.getUniqueId());
//		boolean isManager = StratusAPIPersonal.get().getManagers().contains(player);
//		if (isOwner) {
//			setPrefix(player.getUniqueId(), ownerPrefix);
//		} else if (isManager) {
//			setPrefix(player.getUniqueId(), managerPrefix);
//		} else {
//			super.setDisplayName(player);
//		}
	}

}
