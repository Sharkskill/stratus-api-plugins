/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.personal.task;

import network.stratus.api.personal.StratusAPIPersonal;
import network.stratus.api.pgm.StratusAPIPGM;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * A task which tracks a player's playtime in the current match.
 * 
 * @author Ian Ballingall
 *
 */
public class PullMapsTask extends BukkitRunnable {

	private int cooldown;

	public PullMapsTask() {
		cooldown = 60;
	}

	@Override
	public void run() {
		cooldown--;

		if (cooldown <= 0) {
			cooldown = 120;
			StratusAPIPGM.get().getServer().dispatchCommand(StratusAPIPGM.get().getServer().getConsoleSender(), "newmaps");
		}
	}

}
