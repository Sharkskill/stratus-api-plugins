/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked.commands;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.ranked.StratusAPIRanked;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.pgm.StratusAPIPGM;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Competitor;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.api.player.MatchPlayer;

/**
 * Commands related to Ranked servers.
 * 
 * @author Ian Ballingall
 *
 */
public class RankedCommands {

	@Command(aliases = "invalidate",
			desc = "Do not consider the current match to be a Ranked match",
			perms = "stratusapi.ranked.invalidate")
	public void onInvalidate(CommandSender sender) {
		StratusAPIPGM.get().getParticipantManager().endMatch(PGM.get().getMatchManager().getMatch(sender));
		new SingleAudience(sender).sendMessage("ranked.invalidated");

		StratusAPIPGM.get().getStatisticsManager().getMatchStatistics().getRanked().setEnabled(false);
	}

	@Command(aliases = { "forfeit", "ff" },
			desc = "Request to forfeit the match for your team")
	public void onForfeit(CommandSender sender) {

		MatchPlayer player = PGM.get().getMatchManager().getPlayer((Player) sender);
		Match match = player.getMatch();

		if (!player.getParty().isParticipating()) {
			new SingleAudience(sender).sendMessage("ranked.forfeit.notplaying");
			return;
		}

		if (!match.isRunning()) {
			new SingleAudience(sender).sendMessage("ranked.forfeit.notrunning");
			return;
		}

		if (StratusAPIRanked.get().getForfeits().contains(player.getId())) {
			sender.sendMessage(ChatColor.RED + "You have already ran this command");
			return;
		}
		StratusAPIRanked.get().getForfeits().add(player.getId());
		new MultiAudience.Builder().global().build().sendMessage("ranked.forfeit.add", sender.getName());

		int nonForfeits = 0;
		for (MatchPlayer other : player.getParty().getPlayers()) {
			if (!StratusAPIRanked.get().getForfeits().contains(other.getId())) {
				nonForfeits++;
			}
		}

		if (nonForfeits <= 1) {
			new MultiAudience.Builder().global().build().sendMessage("ranked.forfeit.success", player.getParty().getColor() + player.getParty().getNameLegacy());
			for (Party party : match.getParties()) {
				if (!party.equals(player.getParty()) && party instanceof Competitor) {
					match.finish((Competitor) party);
				}
			}
		}
	}

	@Command(aliases = { "hc", "h" },
			desc = "Send a message to other hosts",
			perms = "stratusapi.ranked.hostchat")
	public void onHostChat(CommandSender sender, @Text String message) {
		new MultiAudience.Builder().includePermission("stratusapi.ranked.hostchat").build().sendMessage(
				"hostchat.message",
				(sender instanceof Player) ? ((Player) sender).getDisplayName() : Chat.OFFLINE_COLOR + "CONSOLE",
				message);
	}

}
