/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked.messaging;

import com.google.common.collect.Iterators;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.models.status.RankedState;
import network.stratus.api.bukkit.models.status.RankedStateMessage;
import network.stratus.api.bukkit.status.StatusUpdater;
import network.stratus.api.messaging.MessageProcessor;
import network.stratus.api.ranked.StratusAPIRanked;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.map.MapInfo;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.countdowns.SingleCountdownContext;
import tc.oc.pgm.cycle.CycleMatchModule;
import tc.oc.pgm.restart.RestartCountdown;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Starts a ranked match
 *
 * @author ShinyDialga
 *
 */
public class RankedMatchMessageProcessor implements MessageProcessor<RankedMatch> {

	public RankedMatchMessageProcessor() {
	}

	@Override
	public void process(RankedMatch object) {
		if (!object.getProxy().equalsIgnoreCase(StratusAPIRanked.get().getProxy())) {
			return;
		}

		if (!object.getServer().equals(StratusAPIRanked.get().getServerName())) {
			return;
		}

		MapInfo map = PGM.get().getMapLibrary().getMap(object.getMap());
		if (map != null) {
			PGM.get().getMapOrder().setNextMap(PGM.get().getMapLibrary().getMap(object.getMap()));
		}

		Match currentMatch = Iterators.getNext(PGM.get().getMatchManager().getMatches(), null);
//		StratusAPIRanked.get().getServer().getScheduler().runTaskLater(StratusAPIRanked.get(), () -> {
//			Match m = Iterators.getNext(PGM.get().getMatchManager().getMatches(), null);
//			if (m != null) {
//				if (!m.isFinished()) {
////					m.finish();
//				}
//				//Run later since there's a chance of observers joining in the middle of cycling.
//				m.needModule(CycleMatchModule.class).startCountdown(Duration.ofSeconds(5));
//			}
//		}, 20L * 60L);
		if (currentMatch != null) {
			if (!currentMatch.isFinished()) {
//				currentMatch.finish();
//				This match tried to auto-finish, but I don't want to do that anymore
			}
			if (!currentMatch.isRunning()) {
				//Run later since there's a chance of observers joining in the middle of cycling.
				currentMatch.needModule(CycleMatchModule.class).startCountdown(Duration.ofSeconds(5));
			}
		}

		StratusAPIRanked.get().setCurrentRankedMatch(object);
		StratusAPIRanked.get().setCurrentMatchStale(false);

		StatusUpdater.get().updateRankedState(
			new RankedStateMessage(
				StratusAPI.get().getServerName(),
				new RankedState(
					object.getRankedId(),
					object.getMap(),
					object.getGamemode(),
					object.isPremium(),
					object.isEloEnabled(),
					object.getSeasonPostfix(),
					object.getCaptain1().stream().map(p -> p.getUuid().toString()).collect(Collectors.toList()),
					object.getCaptain2().stream().map(p -> p.getUuid().toString()).collect(Collectors.toList()),
					object.getTeam1().stream().map(p -> p.getUuid().toString()).collect(Collectors.toList()),
					object.getTeam2().stream().map(p -> p.getUuid().toString()).collect(Collectors.toList()),
					object.getQueueId(),
					object.getChannel1Id(),
					object.getChannel2Id(),
					object.getAvailableMaps(),
					object.getTeam1Veto(),
					object.getTeam2Veto(),
					object.getRankedDiscord(),
					object.getStaffAlertsId(),
					object.getWaitingRoomId()
				)
			)
		);
	}
}
