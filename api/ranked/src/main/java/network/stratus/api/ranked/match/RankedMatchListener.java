/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked.match;

import java.util.Optional;

import static net.kyori.adventure.text.Component.text;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.scheduler.BukkitRunnable;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.match.MatchListener;
import network.stratus.api.ranked.StratusAPIRanked;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.api.player.event.MatchPlayerAddEvent;
import tc.oc.pgm.blitz.BlitzMatchModule;
import tc.oc.pgm.events.PlayerParticipationStartEvent;
import tc.oc.pgm.events.PlayerParticipationStopEvent;
import tc.oc.pgm.teams.Team;

/**
 * Listens on events in order to handle and manage participants in a Ranked
 * match. This includes forcing people onto their registered teams, recording
 * matches starting and ending, preventing team switching and preventing
 * unregistered players joining teams.
 *
 * @author Ian Ballingall
 *
 */
public class RankedMatchListener implements MatchListener {

	private final String discordUrl;

	public RankedMatchListener(String discordUrl) {
		this.discordUrl = discordUrl;
	}

	@Override
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoinTeam(PlayerParticipationStartEvent event) {
		if (!StratusAPIPGM.get().getParticipantManager().isMatchRegistered(event.getMatch()))
			return;

		Player player = Bukkit.getPlayer(event.getPlayer().getId());
//		SingleAudience audience = new SingleAudience(player);
//		if (!event.getMatch().isRunning()) {
//			audience.sendMessage("ranked.punishmentreminder.1");
//			audience.sendMessage("ranked.punishmentreminder.2");
//			return;
//		}

		Optional<Party> party = StratusAPIPGM.get().getParticipantManager().getRegisteredParty(event.getMatch(),
				event.getPlayer().getId());
		if (party.isPresent()) {
			if (!event.getCompetitor().equals(party.get())) {
				String message = String.format(StratusAPI.get().getTranslator().getStringOrDefaultLocale(player.getLocale(),
						"teamchange.rejected.wrongteam"), party.get().getNameLegacy());
				event.cancel(text(message));
			}
		} else {
			String message = StratusAPI.get().getTranslator().getStringOrDefaultLocale(player.getLocale(),
					"ranked.teamchange.rejected.notregistered");
			event.cancel(text(message));
			sendDiscordMessage(player);
		}
	}

	@Override
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerLeaveTeam(PlayerParticipationStopEvent event) {
		if (!StratusAPIPGM.get().getParticipantManager().isMatchRegistered(event.getMatch()))
			return;

		MatchPlayer matchPlayer = event.getPlayer();
		if (matchPlayer != null) {
			Player player = Bukkit.getPlayer(matchPlayer.getId());
			if (
				player != null
					&& player.isOnline()
					&& StratusAPIPGM.get().getParticipantManager().isPlayerRegistered(event.getMatch(), player.getUniqueId())
					&& !event.getMatch().hasModule(BlitzMatchModule.class)
				) {
				String message = StratusAPI.get().getTranslator().getStringOrDefaultLocale(player.getLocale(),
						"ranked.teamchange.rejected");
				event.cancel(text(message));
			}
		}
	}

	@Override
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerEnterMatch(MatchPlayerAddEvent event) {
		if (!StratusAPIPGM.get().getParticipantManager().isMatchRegistered(event.getMatch()))
			return;

		Optional<Party> registeredParty = StratusAPIPGM.get().getParticipantManager().getRegisteredParty(event.getMatch(),
				event.getPlayer().getId());
		if (registeredParty.isPresent()) {
			boolean isTeam = registeredParty.get() instanceof Team;
			// If the registered party is a team that isn't full, put the player on the team
			if (!isTeam || (isTeam && ((Team) registeredParty.get()).getFullness(false) < 1.0f)) {
				event.setInitialParty(registeredParty.get());
			}
		} else {
			sendDiscordMessage(Bukkit.getPlayer(event.getPlayer().getId()));
		}
	}

	/**
	 * Displays the Discord join message to a player after a one tick delay. This is
	 * to ensure it appears below any PGM informational message.
	 *
	 * @param target The player to send the message to
	 */
	// Maybe this belongs elsewhere?
	private void sendDiscordMessage(Player target) {
		new BukkitRunnable() {
			@Override
			public void run() {
				new SingleAudience(target).sendMessage("ranked.discord", discordUrl);
			}
		}.runTaskLater(StratusAPIRanked.get(), 1);
	}

}
