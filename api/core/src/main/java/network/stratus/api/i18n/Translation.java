/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.i18n;

import java.util.Arrays;
import java.util.Locale;

/**
 * Encapsulates a translation.
 * 
 * @author Ian Ballingall
 *
 */
public class Translation {

	private String key;
	private Object[] values;

	public Translation() {
	}

	public Translation(String key, Object[] values) {
		this.key = key;
		this.values = values;
	}

	public String getKey() {
		return key;
	}

	public Object[] getValues() {
		return values;
	}

	/**
	 * Obtain the translated string.
	 * 
	 * @param translator The translator to use for this translation
	 * @param locale     The locale to translate into
	 * @return The translated string
	 */
	public String translate(Translator translator, Locale locale) {
		return String.format(translator.getStringOrDefaultLocale(locale, key), values);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((values == null) ? 0 : Arrays.hashCode(values));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Translation other = (Translation) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!Arrays.equals(values, other.values))
			return false;
		return true;
	}

}
