/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.messaging;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

/**
 * Serialises messages into JSON and publishes them over a {@link Channel}.
 * 
 * @author Ian Ballingall
 * 
 */
public class JsonPublisher extends Publisher {

	/**
	 * Create a publisher with the default settings. This will set the content type
	 * to application/json and encoding to UTF-8.
	 * 
	 * @param channel      The {@link Channel} to transmit messages over
	 * @param exchangeName The exchange to send messages to
	 * @param routingKey   The key which denotes to whom the message should be
	 *                     routed
	 */
	public JsonPublisher(Channel channel, String exchangeName, String routingKey) {
		super(channel, exchangeName, routingKey,
				new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("UTF-8").build());
	}

	/**
	 * Creates a publisher with the given settings. The content type <b>must</b> be
	 * application/json and the encoding <b>must</b> be UTF-8.
	 * 
	 * @param channel      The {@link Channel} to transmit messages over
	 * @param exchangeName The exchange to send messages to
	 * @param routingKey   The key which denotes to whom the message should be
	 *                     routed
	 * @param properties   The settings for this publisher
	 */
	public JsonPublisher(Channel channel, String exchangeName, String routingKey, AMQP.BasicProperties properties) {
		super(channel, exchangeName, routingKey, properties);
		if (!properties.getContentType().equals("application/json"))
			throw new IllegalArgumentException("Content type must be application/json");

		if (!properties.getContentEncoding().equals("UTF-8"))
			throw new IllegalArgumentException("Content encoding must be UTF-8");
	}

	@Override
	public void publish(Object message) throws IOException {
		byte[] data = new ObjectMapper().writeValueAsBytes(message);
		getChannel().basicPublish(getExchangeName(), getRoutingKey(), getProperties(), data);
	}

}
