/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.client;

import javax.ws.rs.core.Response;

import network.stratus.api.i18n.Translation;

/**
 * Represents the body of an error response to a {@link Request}.
 * 
 * @author Ian Ballingall
 *
 */
public class ErrorResponse {

	private String description;
	private Translation translation;

	public String getDescription() {
		return description;
	}

	public Translation getTranslation() {
		return translation;
	}

	/**
	 * Parses a response from the API into an ErrorResponse.
	 * 
	 * @param original The original response object from the API
	 * @return The parsed ErrorResponse
	 */
	public static ErrorResponse parse(Response original) {
		return original.readEntity(ErrorResponse.class);
	}

}
