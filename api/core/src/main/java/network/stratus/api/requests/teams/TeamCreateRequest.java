/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests.teams;

import java.util.UUID;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.responses.ObjectIdResponse;

/**
 * Represents a request to create a new team.
 *
 * @author Meeples10
 */
public class TeamCreateRequest implements Request<ObjectIdResponse> {

	private UUID leader;
	private String name;

	public TeamCreateRequest(UUID leader, String name) {
		this.leader = leader;
		this.name = name;
	}

	public UUID getLeader() {
		return leader;
	}

	public String getName() {
		return name;
	}

	@Override
	public String getEndpoint() {
		return "/teams";
	}

	@Override
	public Class<ObjectIdResponse> getResponseType() {
		return ObjectIdResponse.class;
	}

	@Override
	public ObjectIdResponse make(APIClient client) {
		return client.post(this);
	}

}
