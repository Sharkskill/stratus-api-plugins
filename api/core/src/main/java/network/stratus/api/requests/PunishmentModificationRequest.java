/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.models.punishment.PunishmentFactory;

/**
 * A request to modify the properties of a punishment, based on the target's
 * username and punishment number as seen on /l.
 * 
 * @author Ian Ballingall
 *
 */
public abstract class PunishmentModificationRequest<T extends PunishmentFactory> implements Request<T> {

	@JsonIgnore
	private String username;
	@JsonIgnore
	private int number;

	private UUID sender;
	@JsonProperty("canModifyOthers")
	private boolean canModifyOthers;
	private Map<String, Object> changes;

	protected PunishmentModificationRequest(String username, int number, UUID sender, boolean canModifyOthers,
			Map<String, Object> changes) {
		this.username = username;
		this.number = number;
		this.sender = sender;
		this.canModifyOthers = canModifyOthers;
		this.changes = changes;
	}

	@JsonIgnore
	public String getUsername() {
		return username;
	}

	@JsonIgnore
	public int getNumber() {
		return number;
	}

	public UUID getSender() {
		return sender;
	}

	@JsonProperty("canModifyOthers")
	public boolean canModifyOthers() {
		return canModifyOthers;
	}

	public Map<String, Object> getChanges() {
		return changes;
	}

	@Override
	public String getEndpoint() {
		return "/punishments/name/" + username.replaceAll("/", "%2F") + "/" + number;
	}

	@Override
	public T make(APIClient client) {
		return client.post(this);
	}

	/**
	 * Constructs a new {@link PunishmentModificationRequest}.
	 */
	public static abstract class Builder<T extends PunishmentFactory> {

		protected String username;
		protected int number;
		protected UUID sender;
		protected boolean canModifyOthers = false;
		protected HashMap<String, Object> changes = new HashMap<>();

		/**
		 * Instantiate the builder.
		 * 
		 * @param username        The target username
		 * @param number          The target punishment number
		 * @param sender          The sender's {@link UUID}, or {@code null} for console
		 * @param canModifyOthers Whether the sender can modify punishments they didn't
		 *                        issue
		 */
		public Builder(String username, int number, UUID sender, boolean canModifyOthers) {
			this.username = username;
			this.number = number;
			this.sender = sender;
			this.canModifyOthers = canModifyOthers;
		}

		/**
		 * Change the punishment's active status. Inactive punishments are considered
		 * appealed.
		 * 
		 * @param active The new active status
		 * @return The builder
		 */
		public Builder<T> active(boolean active) {
			changes.put("active", active);
			return this;
		}

		/**
		 * Change the punishment's type.
		 * 
		 * @param type The new punishment type
		 * @return The builder
		 */
		public Builder<T> type(String type) {
			changes.put("type", type.toUpperCase());
			return this;
		}

		/**
		 * Set the punishment's duration. For normal punishments, this is from the
		 * punishment's issue time. For timed punishments, this sets the remaining time.
		 * 
		 * @param duration The new duration, in string format
		 * @return The builder
		 */
		public Builder<T> duration(String duration) {
			changes.put("duration", duration);
			return this;
		}

		/**
		 * Set the punishment's reason.
		 * 
		 * @param reason The new reason
		 * @return The builder
		 */
		public Builder<T> reason(String reason) {
			changes.put("reason", reason);
			return this;
		}

		/**
		 * Construct the {@link PunishmentModificationRequest}.
		 * 
		 * @return The request
		 */
		public abstract PunishmentModificationRequest<T> build();

	}

}
