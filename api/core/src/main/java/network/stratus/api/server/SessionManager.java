/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.server;

import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.RequestFailureException;
import network.stratus.api.models.Session;

/**
 * Manages {@link Session}s for players on a server.
 *
 * @author Ian Ballingall
 */
public class SessionManager {

	private final Map<UUID, Session> sessions;
	private final Map<UUID, Lock> locks;

	public SessionManager() {
		this.sessions = new ConcurrentHashMap<>();
		this.locks = new ConcurrentHashMap<>();
	}

	/**
	 * Get the current {@link Session} for a given player.
	 *
	 * @param uuid The player's {@link UUID}
	 * @return An {@link Optional} containing the {@link Session}, if one exists
	 */
	public Optional<Session> getSession(UUID uuid) {
		return Optional.ofNullable(sessions.get(uuid));
	}

	/**
	 * Store a new {@link Session} for a given player. This does not save the
	 * session to the database - to save a new session, use {@link this#saveNewSession(Session, APIClient)}.
	 *
	 * @param session The {@link Session} to store
	 * @return An {@link Optional} containing the player's previous {@link Session},
	 * if it exists
	 */
	public Optional<Session> addSession(Session session) {
		return Optional.ofNullable(sessions.put(session.getPlayerUuid(), session));
	}

	/**
	 * Remove the {@link Session} stored for a given player. This does not
	 * terminate the session in the database - to terminate a session, use {@link this#terminateSession(UUID, APIClient)}
	 * or {@link this#terminateSession(UUID, APIClient, Supplier)}.
	 *
	 * @param uuid The player's {@link UUID}
	 * @return An {@link Optional} containing the removed {@link Session}, if it
	 * exists
	 */
	public Optional<Session> removeSession(UUID uuid) {
		return Optional.ofNullable(sessions.remove(uuid));
	}

	/**
	 * Saves the given {@link Session} to the API using the given {@link APIClient}
	 * and records it in the manager. If a previous session already exists, it will
	 * be terminated. This is done in a safe manner, locking against the player's {@link UUID}.
	 *
	 * @param session The {@link Session} to save
	 * @param client  The {@link APIClient} to use to save it
	 * @return The updated {@link Session} from the API
	 */
	public Session saveNewSession(Session session, APIClient client) {
		Lock lock = locks.computeIfAbsent(session.getPlayerUuid(), k -> new ReentrantLock());
		lock.lock();
		try {
			// First, try to save previous session if it exists
			removeSession(session.getPlayerUuid()).ifPresent(s -> {
				try { // Separate try - even if this fails, we'll try to save the new session
					if (s.getEndTime() == null)
						s.setEndTime(new Date());
					s.make(client);
				} catch (RequestFailureException e) {
					System.err.println(String.format("Failed to save previous session %s for %s: %s",
							s.get_id(), s.getPlayerUuid(), e));
					e.printStackTrace();
				}
			});

			session = session.make(client);
			addSession(session); // Store result returned from API
		} catch (RequestFailureException e) {
			System.err.println(String.format("Failed to save session %s for %s: %s",
					session.get_id(), session.getPlayerUuid(), e));
			e.printStackTrace();
			addSession(session); // If saving failed, store the session so we can try again on disconnect
		} finally {
			lock.unlock();
		}

		return session;
	}

	/**
	 * Terminate a player's currently active session. This is done in a safe manner,
	 * locking against the player's {@link UUID}.
	 *
	 * @param uuid   The player's {@link UUID}
	 * @param client {@link APIClient} to use to save it
	 * @return An {@link Optional} containing the terminated {@link Session}
	 */
	public Optional<Session> terminateSession(UUID uuid, APIClient client) {
		return terminateSession(uuid, client, () -> null);
	}

	/**
	 * Terminate a player's current active session. If one does not exist, the given
	 * {@link Supplier} will be used to generate a 'stub' session. This is done in a safe
	 * manner, locking against the player's {@link UUID}.
	 *
	 * @param uuid   The player's {@link UUID}
	 * @param client The {@link APIClient} to use to save it
	 * @param stub   The stub {@link Supplier} to be used if no active session exists
	 * @return An {@link Optional} containing the terminated {@link Session}
	 */
	public Optional<Session> terminateSession(UUID uuid, APIClient client, Supplier<Session> stub) {
		Lock lock = locks.computeIfAbsent(uuid, k -> new ReentrantLock());
		lock.lock();
		Session session = null;
		try {
			session = removeSession(uuid).orElseGet(stub);
			if (session != null) {
				session.setEndTime(new Date());
				session.make(client);
			}
		} catch (RequestFailureException e) {
			if (session != null) {
				addSession(session);
				System.err.println(String.format("Failed to terminate session %s for %s: %s",
						session.get_id(), session.getPlayerUuid(), e));
				e.printStackTrace();
			}
		} finally {
			lock.unlock();
		}

		return Optional.ofNullable(session);
	}

}
