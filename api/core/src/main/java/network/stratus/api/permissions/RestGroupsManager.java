/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.permissions;

import java.util.List;

import network.stratus.api.client.APIClient;
import network.stratus.api.requests.GroupIdRequest;
import network.stratus.api.requests.GroupNameRequest;

/**
 * A {@link MappedGroupsManager} which uses a
 * {@link network.stratus.api.client.APIClient} to retrieve group data.
 * 
 * @author Ian Ballingall
 *
 */
public class RestGroupsManager extends MappedGroupsManager {

	private APIClient apiClient;

	public RestGroupsManager(List<String> defaultGroupNames, APIClient apiClient) {
		super(defaultGroupNames);
		this.apiClient = apiClient;
	}

	public APIClient getApiClient() {
		return apiClient;
	}

	@Override
	public Group getStoredGroupById(String id) {
		Group group = getCachedGroupById(id);
		if (group == null) {
			group = new GroupIdRequest(id).make(apiClient);
			addGroup(group);
		}

		return group;
	}

	@Override
	public Group getStoredGroupByName(String name) {
		Group group = getCachedGroupByName(name);
		if (group == null) {
			group = new GroupNameRequest(name).make(apiClient);
			addGroup(group);
		}

		return group;
	}

}
