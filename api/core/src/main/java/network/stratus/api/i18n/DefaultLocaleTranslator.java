/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.i18n;

import java.util.Locale;
import java.util.Optional;

/**
 * A translator which operates only for the given manager's default locale. Any
 * provided {@link Locale}s are ignored in favour of the default. This is
 * largely intended for development.
 * 
 * @author Ian Ballingall
 *
 */
public class DefaultLocaleTranslator implements Translator {

	private LanguageManager manager;

	public DefaultLocaleTranslator(LanguageManager manager) {
		this.manager = manager;
	}

	@Override
	public boolean hasString(Locale locale, String key) {
		return manager.getDefaultLanguage().hasString(key);
	}

	@Override
	public Optional<String> getString(Locale locale, String key) {
		return manager.getDefaultLanguage().getString(key);
	}

	@Override
	public String getStringOrDefaultLocale(Locale locale, String key) {
		return getString(locale, key).get();
	}

	@Override
	public String getStringOrDefault(Locale locale, String key, String defaultOption) {
		return getString(locale, key).orElse(defaultOption);
	}

	@Override
	public Locale getDefaultLocale() {
		return manager.getDefaultLocale();
	}

}
