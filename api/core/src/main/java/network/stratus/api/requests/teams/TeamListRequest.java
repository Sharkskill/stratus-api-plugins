/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests.teams;

import java.util.HashMap;
import java.util.Map;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.responses.teams.TeamListResponse;

/**
 * Represents a request to list the teams.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamListRequest implements Request<TeamListResponse> {

	private int page;

	public TeamListRequest(int page) {
		this.page = page;
	}

	@Override
	public String getEndpoint() {
		return "/teams";
	}

	@Override
	public Class<TeamListResponse> getResponseType() {
		return TeamListResponse.class;
	}

	@Override
	public TeamListResponse make(APIClient client) {
		return client.get(this);
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> properties = new HashMap<>();
		properties.put("page", page);
		return properties;
	}

}
