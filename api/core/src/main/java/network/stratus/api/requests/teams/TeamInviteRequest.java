/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests.teams;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.responses.teams.TeamTargetInfoResponse;

/**
 * Represents a request to invite someone to a team.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamInviteRequest implements Request<TeamTargetInfoResponse> {

	private UUID sender;
	private String target;
	@JsonIgnore
	private boolean create;

	public TeamInviteRequest(UUID sender, String target, boolean create) {
		this.sender = sender;
		this.target = target;
		this.create = create;
	}

	public UUID getSender() {
		return sender;
	}

	public String getTarget() {
		return target;
	}

	@JsonIgnore
	public boolean getCrate() {
		return create;
	}

	@Override
	public String getEndpoint() {
		return "/teams/invite/" + sender + "/" + target;
	}

	@Override
	public Class<TeamTargetInfoResponse> getResponseType() {
		return TeamTargetInfoResponse.class;
	}

	@Override
	public TeamTargetInfoResponse make(APIClient client) {
		return create ? client.put(this) : client.delete(this);
	}

}
