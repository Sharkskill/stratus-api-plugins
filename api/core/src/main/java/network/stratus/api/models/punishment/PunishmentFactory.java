/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.models.punishment;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import network.stratus.api.models.User;

/**
 * Abstract representation of a factory which manufactures the appropriate type
 * of {@link Punishment}, which can then be enforced.
 * 
 * @author Ian Ballingall
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class PunishmentFactory {

	public PunishmentFactory() {
	}

	public PunishmentFactory(String _id, User issuer, User target,
			Type type, Date time, Date expiry, String reason, boolean active, int number, long timeRemaining,
			String serverName) {
		this._id = _id;
		this.issuer = issuer;
		this.target = target;
		this.type = type;
		this.time = time;
		this.expiry = expiry;
		this.reason = reason;
		this.active = active;
		this.number = number;
		this.timeRemaining = timeRemaining;
		this.serverName = serverName;
	}

	/** The unique ID of this punishment. */
	protected String _id;
	/** The e punishment issuer. */
	protected User issuer;
	/** The punishment target. */
	protected User target;
	/** The type of punishment. */
	protected Type type;
	/** The time of issue. */
	protected Date time;
	/** The expiry time. */
	protected Date expiry;
	/** The reason for the punishment. */
	protected String reason;
	/** Whether this punishment is active. */
	protected boolean active;
	/** The number of this punishment for this player. */
	protected int number;
	/** The time remaining for this punishment, if it is a timed punishment. */
	protected long timeRemaining;
	/** The name of the server on which this punishment was issued. */
	protected String serverName;
	/** Whether this punishment is only visible to staff. */
	protected boolean silent;

	/**
	 * Represents the type of a punishment.
	 */
	public enum Type {
		WARN("WARN"), KICK("KICK"), BAN("BAN"), MUTE("MUTE"),
		/**
		 * Used to denote that the punishment type should be determined automatically.
		 */
		AUTO("AUTO");

		private String type;
		private static final Set<Type> TIMED_TYPES = Collections
				.unmodifiableSet(new HashSet<>(Collections.singletonList(MUTE)));

		private Type(String type) {
			this.type = type;
		}

		/**
		 * Get the string representation of this punishment type.
		 * 
		 * @return The string representation of this punishment type
		 */
		public String getType() {
			return type;
		}

		/**
		 * Whether this type corresponds to a timed punishment.
		 * 
		 * @return If this type is timed
		 */
		public boolean isTimedType() {
			return TIMED_TYPES.contains(this);
		}
	}

	public String get_id() {
		return _id;
	}

	public User getIssuer() {
		return issuer;
	}

	public User getTarget() {
		return target;
	}

	public Type getType() {
		return type;
	}

	public Date getTime() {
		return time;
	}

	public Date getExpiry() {
		return expiry;
	}

	public String getReason() {
		return reason;
	}

	public boolean isActive() {
		return active;
	}

	public int getNumber() {
		return number;
	}

	public String getServerName() {
		return serverName;
	}

	public long getTimeRemaining() {
		return timeRemaining;
	}

	public boolean isSilent() {
		return silent;
	}

	/**
	 * Obtain the {@link Punishment} corresponding to the configured fields.
	 * 
	 * @return The punishment of the appropriate type
	 */
	public abstract Punishment getObject();

}
