/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests.teams;

import java.util.UUID;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.models.Team;

/**
 * Represents a reply to a team invite.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamInviteReplyRequest implements Request<Team> {

	private UUID sender;
	private String team;
	private boolean accept;

	public TeamInviteReplyRequest(UUID sender, String team, boolean accept) {
		this.sender = sender;
		this.team = team;
		this.accept = accept;
	}

	public UUID getSender() {
		return sender;
	}

	public String getTeam() {
		return team;
	}

	public boolean isAccept() {
		return accept;
	}

	@Override
	public String getEndpoint() {
		return "/teams/invite/reply";
	}

	@Override
	public Class<Team> getResponseType() {
		return Team.class;
	}

	@Override
	public Team make(APIClient client) {
		return client.post(this);
	}

}
