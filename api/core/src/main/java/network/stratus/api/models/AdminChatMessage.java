/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.models;

import java.util.UUID;

/**
 * Represents an admin chat message transmitted over a messaging service.
 * 
 * @author Ian Ballingall
 *
 */
public class AdminChatMessage {

	private UUID sender;
	private String senderName;
	private String serverName;
	private String message;

	public AdminChatMessage() {
	}

	public AdminChatMessage(UUID sender, String senderName, String serverName, String message) {
		this.sender = sender;
		this.senderName = senderName;
		this.serverName = serverName;
		this.message = message;
	}

	public UUID getSender() {
		return sender;
	}

	public String getSenderName() {
		return senderName;
	}

	public String getServerName() {
		return serverName;
	}

	public String getMessage() {
		return message;
	}

}
