/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.i18n;

import java.util.Locale;
import java.util.Map;
import java.util.Optional;

/**
 * Represents a language, which contains a set of localised strings.
 * 
 * @author Ian Ballingall
 *
 */
public class Language {

	/** The locale represented by this language. */
	private Locale locale;
	/** The set of strings in this language. */
	private Map<String, String> strings;

	public Language(Locale locale, Map<String, String> strings) {
		this.locale = locale;
		this.strings = strings;
	}

	/**
	 * Get the locale this language represents.
	 * 
	 * @return The locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Get the friendly display name for this language.
	 * 
	 * @return The language's display name
	 */
	public String getDisplayName() {
		return locale.getDisplayName();
	}

	/**
	 * Check if this language has the given string.
	 * 
	 * @param key The key for the string
	 * @return Whether this language has this string
	 */
	public boolean hasString(String key) {
		return strings.containsKey(key);
	}

	/**
	 * Get the given translated string from this language.
	 * 
	 * @param key The key for the string
	 * @return An Optional representing the translated string
	 */
	public Optional<String> getString(String key) {
		return Optional.ofNullable(strings.get(key));
	}

}
