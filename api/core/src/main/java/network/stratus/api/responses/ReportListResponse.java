/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.responses;

import java.util.List;
import network.stratus.api.models.Report;
import network.stratus.api.models.User;

/**
 * Represents a response to a
 * {@link network.stratus.api.requests.ReportListRequest}.
 * 
 * @author Ian Ballingall
 *
 */
public class ReportListResponse {

	private boolean allServers;
	private String server;
	private User target;
	private List<Report> list;

	public boolean getAllServers() {
		return allServers;
	}

	public String getServer() {
		return server;
	}

	public User getTarget() {
		return target;
	}

	public List<Report> getList() {
		return list;
	}

}
