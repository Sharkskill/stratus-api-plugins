/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.permissions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represents a permission group. Nullable indicates that the value may be null
 * after it has been fully initialised from its storage representation.
 * 
 * @author Ian Ballingall
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Group implements Comparable<Group> {

	/** The unique ID in the database for this group. */
	private String _id;
	/** The unique name for this group. */
	private String name;
	/** The chat prefix for this group. */
	private @Nullable String globalPrefix;
	/** The priority for this group, where lower is more important. */
	private int priority;
	/** Whether this group affects tab ordering. */
	private boolean ordered;
	/** The map of realm-specific prefixes. */
	private @Nullable Map<String, Prefix> prefixes;
	/** The {@link Map} of permission realms to permission nodes. */
	private @Nullable Map<String, List<String>> minecraftPermissions;

	public String get_id() {
		return _id;
	}

	public String getName() {
		return name;
	}

	@Nullable
	public String getGlobalPrefix() {
		return globalPrefix;
	}

	public int getPriority() {
		return priority;
	}

	public boolean isOrdered() {
		return ordered;
	}

	@Nullable
	public Map<String, Prefix> getPrefixes() {
		return prefixes;
	}

	@Nullable
	public Map<String, Prefix> getPrefixes(Collection<String> realms) {
		if (prefixes == null)
			return null;

		return prefixes.entrySet().stream().filter(e -> realms.contains(e.getKey())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	@Nullable
	public Map<String, List<String>> getMinecraftPermissions() {
		return minecraftPermissions;
	}

	/**
	 * Get the list of all permissions across all realms attached to this group.
	 * 
	 * @return The {@link List} of permission nodes
	 */
	public List<String> getPermissions() {
		if (minecraftPermissions == null)
			return new ArrayList<>();

		List<String> permissions = new ArrayList<>();
		for (List<String> realmPermissions : minecraftPermissions.values()) {
			permissions.addAll(realmPermissions);
		}

		return permissions;
	}

	/**
	 * Get the list of permission nodes for the given realm.
	 * 
	 * @param realm The realm whose nodes will be returned
	 * @return The {@link List} of permission nodes
	 */
	public List<String> getPermissions(String realm) {
		if (minecraftPermissions == null)
			return new ArrayList<>();

		return minecraftPermissions.getOrDefault(realm, new ArrayList<String>());
	}

	/**
	 * Get the list of permission nodes for the given set of realms.
	 * 
	 * @param realms The realms whose nodes will be returned
	 * @return The {@link List} of permission nodes
	 */
	public List<String> getPermissions(Collection<String> realms) {
		if (minecraftPermissions == null)
			return new ArrayList<>();

		List<String> permissions = new ArrayList<>();
		for (String realm : realms) {
			permissions.addAll(getPermissions(realm));
		}

		return permissions;
	}

	/**
	 * Compare the relative order of two groups, based on their priority. A group
	 * with a lower priority number will be treated as 'larger' than this group,
	 * giving a positive result.<br>
	 * <br>
	 * 
	 * Note: this class has a natural ordering that is inconsistent with equals. If
	 * two distinct groups of equal priority are compared, one will win, though
	 * consistency is not guaranteed.
	 * 
	 * @param other The group to compare with
	 */
	@Override
	public int compareTo(Group other) {
		int comparison = Integer.compare(other.priority, priority);
		if (comparison == 0) {
			// If priority equal, we check if they are the same group; if not, one wins
			return this.equals(other) ? 0 : 1;
		} else {
			// Otherwise, we return the priority-based comparison for ordering
			return comparison;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (_id == null) {
			if (other._id != null)
				return false;
		} else if (!_id.equals(other._id))
			return false;
		return true;
	}

	/**
	 * Represents a prefix corresponding to a given realm.
	 */
	public static class Prefix implements Comparable<Prefix> {

		private String prefix;
		private boolean override;
		private int priority;

		public Prefix() {}

		public Prefix(String prefix, boolean override, int priority) {
			this.prefix = prefix;
			this.override = override;
			this.priority = priority;
		}

		public String getPrefix() {
			return prefix;
		}

		public boolean isOverride() {
			return override;
		}

		public int getPriority() {
			return priority;
		}

		@Override
		public int compareTo(Prefix o) {
			return Integer.compare(o.priority, priority);
		}

	}

}
