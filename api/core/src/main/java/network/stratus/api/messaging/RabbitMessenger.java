/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.messaging;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ExceptionHandler;

/**
 * Encapsulates the RabbitMQ connection.
 * 
 * @author Ian Ballingall
 *
 */
public class RabbitMessenger {

	private String uri;
	private Connection connection;
	private ExceptionHandler exceptionHandler;

	public RabbitMessenger(String uri, ExceptionHandler exceptionHandler) {
		this.uri = uri;
		this.exceptionHandler = exceptionHandler;
	}

	public String getUri() {
		return uri;
	}

	/**
	 * Initialises the {@link Connection} to the RabbitMQ server. If the connection
	 * is already established, nothing happens. The connection will automatically
	 * recover upon failure and will remain open until closed with
	 * {@link RabbitMessenger#closeConnection()}.
	 * 
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws URISyntaxException
	 * @throws IOException
	 * @throws TimeoutException
	 * @throws KeyStoreException
	 * @throws CertificateException
	 * @throws UnrecoverableKeyException
	 */
	public void initialiseConnection() throws KeyManagementException, NoSuchAlgorithmException, URISyntaxException,
			IOException, TimeoutException, KeyStoreException, CertificateException, UnrecoverableKeyException {
		if (connection != null && connection.isOpen())
			return;

		ConnectionFactory cf = new ConnectionFactory();
		cf.setUri(uri);
		cf.setAutomaticRecoveryEnabled(true);
		cf.setExceptionHandler(exceptionHandler);

		if (uri.startsWith("ampqs://")) {
			KeyStore ks = KeyStore.getInstance(System.getProperty("javax.net.ssl.keyStoreType"));
			ks.load(new FileInputStream(System.getProperty("javax.net.ssl.keyStore")),
					System.getProperty("javax.net.ssl.keyStorePassword").toCharArray());
			KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			kmf.init(ks, System.getProperty("javax.net.ssl.keyStorePassword").toCharArray());

			KeyStore tks = KeyStore.getInstance(System.getProperty("javax.net.ssl.trustStoreType"));
			tks.load(new FileInputStream(System.getProperty("javax.net.ssl.trustStore")),
					System.getProperty("javax.net.ssl.trustStorePassword").toCharArray());
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(tks);

			SSLContext c = SSLContext.getDefault();
			c.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

			cf.useSslProtocol(c);
			cf.enableHostnameVerification();
		}

		connection = cf.newConnection();
	}

	/**
	 * Closes the RabbitMQ {@link Connection}, if it exists.
	 */
	public void closeConnection() {
		if (connection != null) {
			// It isn't that important if this fails, it probably means the connection
			// wasn't open, so we don't need to pass the exception on
			try {
				connection.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Obtain a new RabbitMQ {@link Channel} which operates over the
	 * {@link Connection}. Ensure the connection has been initialised before
	 * obtaining a channel.
	 * 
	 * @return A new {@link Channel}
	 * @throws IOException If a problem occurs creating a new {@link Channel}
	 */
	public Channel getNewChannel() throws IOException {
		Channel channel = connection.createChannel();
		if (channel == null)
			throw new IOException("Failed to allocate new channel");

		return channel;
	}

}
