/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.models.server.ServerStatus;
import network.stratus.api.responses.ServerResponse;

/**
 * A request to change the online status of the server.
 * 
 * @author Ian Ballingall
 *
 */
public class ServerStatusChangeRequest implements Request<ServerResponse> {

	private ServerStatus status;

	public ServerStatusChangeRequest(ServerStatus status) {
		this.status = status;
	}

	public ServerStatus getStatus() {
		return status;
	}

	@Override
	public String getEndpoint() {
		return "/servers/status";
	}

	@Override
	public Class<ServerResponse> getResponseType() {
		return ServerResponse.class;
	}

	@Override
	public ServerResponse make(APIClient client) {
		return client.post(this);
	}

}
