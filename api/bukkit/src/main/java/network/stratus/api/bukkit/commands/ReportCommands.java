/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import app.ashcon.intake.Command;
import app.ashcon.intake.parametric.annotation.Default;
import app.ashcon.intake.parametric.annotation.Range;
import app.ashcon.intake.parametric.annotation.Switch;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.BukkitReport;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.server.CooldownManager;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.models.Report;
import network.stratus.api.models.User;
import network.stratus.api.requests.ReportListRequest;
import network.stratus.api.responses.ReportListResponse;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Date;

/**
 * Commands for reporting players and viewing reports.
 * 
 * @author Ian Ballingall
 *
 */
public class ReportCommands {

	private CooldownManager reportCooldown;

	public ReportCommands(long cooldownTime) {
		this.reportCooldown = new CooldownManager(cooldownTime);
	}

	@Command(aliases = "report",
			desc = "Report a player who is breaking the rules",
			perms = "stratusapi.command.report",
			usage = "<player> <reason>")
	public void onReport(CommandSender sender, Player target, @Text String reason) {
		final User reporter;
		if (sender instanceof Player) {
			Player reporterPlayer = (Player) sender;
			reporter = new User(reporterPlayer.getUniqueId(), reporterPlayer.getName());

			if (!reportCooldown.executeIfAllowed(reporterPlayer.getUniqueId())) {
				new SingleAudience(sender).sendMessage("report.cooldown",
						(reportCooldown.getLastExecutionTime(reporterPlayer.getUniqueId()) + reportCooldown.getCooldownTime()
								- System.currentTimeMillis()) / 1000);
				return;
			}
		} else {
			reporter = User.CONSOLE; // Ensure consistent format of User
		}

		BukkitReport report = new BukkitReport(reporter, new User(target.getUniqueId(), target.getName()), new Date(), reason,
				StratusAPI.get().getServerName());

		StratusAPI.get().newSharedChain("reports").<Report>asyncFirst(() -> {
			return report.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			new SingleAudience(sender).sendMessage("report.submitted");
			report.broadcast(false);
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "reports",
			desc = "View the list of reports",
			perms = "stratusapi.command.reports",
			usage = "[-a] [-p <player>] [-s <server>] [<page>]",
			flags = "asp")
	public void listReports(CommandSender sender, @Default("1") @Range(min = 1) int page,
			@Switch('p') TabCompletePlayer player, @Switch('s') String server, @Switch('a') boolean allServers) {
		ReportListRequest request = new ReportListRequest(allServers, server, player.getUsername(), page);

		StratusAPI.get().newSharedChain("reports").<ReportListResponse>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getAllServers()) {
				audience.sendMessage("report.list.title.allservers");
			} else if (response.getTarget() == null) {
				audience.sendMessage("report.list.title", response.getServer());
			} else {
				Player target = StratusAPI.get().getServer().getPlayer(response.getTarget().get_id());
				audience.sendMessage("report.list.title",
						(target == null) ? Chat.OFFLINE_COLOR + response.getTarget().getUsername()
								: target.getDisplayName());
			}

			for (Report report : response.getList()) {
				Player reporter = StratusAPI.get().getServer().getPlayer(report.getReporter().get_id());
				Player target = StratusAPI.get().getServer().getPlayer(report.getTarget().get_id());

				if (allServers) {
					audience.sendMessage("report.list.entry.includeserver", report.getServerName(),
							StratusAPI.get().getTranslator().getTimeDifferenceString(sender.getLocale(),
									report.getTime(), new Date()),
							(reporter == null) ? Chat.OFFLINE_COLOR + report.getReporter().getUsername()
									: reporter.getDisplayName(),
							(target == null) ? Chat.OFFLINE_COLOR + report.getTarget().getUsername()
									: target.getDisplayName(),
							report.getReason());
				} else {
					audience.sendMessage("report.list.entry",
							StratusAPI.get().getTranslator().getTimeDifferenceString(sender.getLocale(),
									report.getTime(), new Date()),
							(reporter == null) ? Chat.OFFLINE_COLOR + report.getReporter().getUsername()
									: reporter.getDisplayName(),
							(target == null) ? Chat.OFFLINE_COLOR + report.getTarget().getUsername()
									: target.getDisplayName(),
							report.getReason());
				}
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

}
