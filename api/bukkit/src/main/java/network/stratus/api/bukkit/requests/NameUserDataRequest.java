/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests;

import java.util.List;

import network.stratus.api.models.punishment.PunishmentFactory.Type;

/**
 * A {@link UserDataRequest} given a username.
 * 
 * @author Ian Ballingall
 *
 */
public class NameUserDataRequest extends UserDataRequest {

	private String username;

	protected NameUserDataRequest(String username, List<String> data, List<Type> punishmentTypes) {
		super(data, punishmentTypes);
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	@Override
	public String getEndpoint() {
		return "/players/name/" + username.replaceAll("/", "%2F");
	}

	public static class Builder extends UserDataRequest.Builder {

		protected String username;

		public Builder(String username) {
			super();
			this.username = username;
		}

		@Override
		public UserDataRequest build() {
			return new NameUserDataRequest(username, data, punishmentTypes);
		}

	}

}
