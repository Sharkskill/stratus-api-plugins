/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.chat;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nullable;

import org.bukkit.scheduler.BukkitTask;

import network.stratus.api.models.punishment.TimedPunishment;

/**
 * Manages the storage of mutes for online players.
 * 
 * @author Ian Ballingall
 *
 */
public class MuteManager {

	private Map<UUID, TimedPunishment> mutes;
	private Set<String> mutedCommands;
	private Map<UUID, BukkitTask> muteTasks;

	private boolean onlineTimeIsGameplay;
	private long updatePeriod;

	public MuteManager(Set<String> mutedCommands, boolean onlineTimeIsGameplay, long updatePeriod) {
		this.mutes = new ConcurrentHashMap<>();
		this.mutedCommands = mutedCommands;
		this.muteTasks = new ConcurrentHashMap<>();
		this.onlineTimeIsGameplay = onlineTimeIsGameplay;
		this.updatePeriod = updatePeriod;
	}

	/**
	 * Records a new mute against the target.
	 * 
	 * @param mute The mute to store
	 */
	public void addMute(TimedPunishment mute) {
		mutes.put(mute.getTarget().get_id(), mute);
	}

	/**
	 * Removes the mute from local storage.
	 * 
	 * @param uuid The UUID of the player whose mute to remove
	 * @return The mute that was removed
	 */
	@Nullable
	public TimedPunishment removeMute(UUID uuid) {
		return mutes.remove(uuid);
	}

	/**
	 * Gets the locally stored mute for the given player.
	 * 
	 * @param uuid The UUID of the player
	 * @return The player's mute
	 */
	@Nullable
	public TimedPunishment getMute(UUID uuid) {
		return mutes.get(uuid);
	}

	/**
	 * Gets the locally stored active mute for the given player. If a mute is stored
	 * that is inactive or expired, it will be removed from local storage and will
	 * not be returned.
	 * 
	 * @param uuid The UUID of the player
	 * @return The player's active, unexpired mute
	 */
	@Nullable
	public TimedPunishment getActiveMute(UUID uuid) {
		TimedPunishment mute = mutes.get(uuid);
		if (mute != null && (!mute.isActive() || (mute.getExpiry() != null && mute.getExpiry().before(new Date()))
				|| (mute.getTimeRemaining() <= 0))) {
			removeMute(uuid);
			return null;
		} else {
			return mute;
		}
	}

	/**
	 * Reports whether this command is to be blocked for muted players.
	 * 
	 * @param command The command
	 * @return Whether this command is muted
	 */
	public boolean isCommandMuted(String command) {
		return mutedCommands.contains(command);
	}

	/**
	 * Whether or not being online on this server counts towards mutes' gameplay
	 * time.
	 * 
	 * @return Whether or not being online on this server counts towards mutes'
	 *         gameplay time
	 */
	public boolean isOnlineTimeGameplay() {
		return onlineTimeIsGameplay;
	}

	/**
	 * How often mutes' durations are updated in milliseconds.
	 * 
	 * @return How often mutes' durations are updated in milliseconds
	 */
	public long getUpdatePeriod() {
		return updatePeriod;
	}

	/**
	 * Stores a task which is used to track and update timed mutes. If one already
	 * exists, it will be cancelled and replaced.
	 * 
	 * @param uuid The UUID of the punished player
	 * @param task The task
	 */
	public void addMuteTask(UUID uuid, BukkitTask task) {
		BukkitTask oldTask = muteTasks.put(uuid, task);
		if (oldTask != null) {
			oldTask.cancel();
		}
	}

	/**
	 * Removes the task corresponding to the given player, if it exists.
	 * 
	 * @param uuid The UUID of the player
	 */
	public void removeMuteTask(UUID uuid) {
		BukkitTask task = muteTasks.remove(uuid);
		if (task != null) {
			task.cancel();
		}
	}

}
