/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands.providers;

import java.lang.annotation.Annotation;
import java.util.List;

import javax.annotation.Nullable;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.argument.ArgumentException;
import app.ashcon.intake.argument.CommandArgs;
import app.ashcon.intake.argument.Namespace;
import app.ashcon.intake.bukkit.parametric.provider.BukkitProvider;
import app.ashcon.intake.bukkit.parametric.provider.DynamicPlayerProvider;
import app.ashcon.intake.bukkit.util.BukkitUtil;
import app.ashcon.intake.parametric.ProvisionException;
import app.ashcon.intake.parametric.annotation.Default;
import app.ashcon.intake.parametric.annotation.Switch;
import network.stratus.api.bukkit.models.TabCompletePlayer;

/**
 * A {@link BukkitProvider} which allows for tab completion of players who are
 * online, yet accepts usernames if no such player is found. This is intended
 * for commands which send requests to the backend, so that they can support tab
 * completion and offline players.
 * 
 * @author Ian Ballingall
 *
 */
public class PlayerTabProvider implements BukkitProvider<TabCompletePlayer> {

	private DynamicPlayerProvider playerProvider = new DynamicPlayerProvider();

	@Override
	public String getName() {
		return "username";
	}

	@Override
	public TabCompletePlayer get(CommandSender sender, CommandArgs args, List<? extends Annotation> mods)
			throws ArgumentException, ProvisionException {
		if (args.hasNext()) {
			String username = args.next();
			Player player = BukkitUtil.getPlayer(username, sender);
			return new TabCompletePlayer(username, player);
		}

		for (Annotation mod : mods) {
			if (mod instanceof Nullable)
				return null;

			if (mod instanceof Switch)
				return new TabCompletePlayer("");

			if (mod instanceof Default)
				return new TabCompletePlayer(((Default) mod).value()[0]);
		}

		throw new ArgumentException("You must provide a username");
	}

	@Override
	public List<String> getSuggestions(String prefix, CommandSender sender, Namespace namespace,
			List<? extends Annotation> mods) {
		return playerProvider.getSuggestions(prefix, sender, namespace, mods);
	}

}
