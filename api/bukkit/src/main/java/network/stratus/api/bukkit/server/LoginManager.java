/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.server;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.entity.Player;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.models.LoginData;

/**
 * Manages {@link Player}s' login data.
 * 
 * @author Ian Ballingall
 *
 */
public class LoginManager {

	private Map<UUID, LoginData> loginData;
	private Map<UUID, BukkitTask> cleanupTasks;
	/** The delay before a {@link Player}'s login data is cleaned up in ticks. */
	private static final int CLEANUP_DELAY = 6000;

	public LoginManager() {
		this.loginData = new ConcurrentHashMap<>();
		this.cleanupTasks = new ConcurrentHashMap<>();
	}

	/**
	 * Obtain the {@link LoginData} associated with the given {@link Player} without
	 * removing it.
	 * 
	 * @param uuid The {@link Player}'s {@link UUID}
	 * @return Their {@link LoginData}, if it exists
	 */
	public LoginData getLoginData(UUID uuid) {
		return loginData.get(uuid);
	}

	/**
	 * Obtain the {@link LoginData} associated with the given {@link Player},
	 * removing it from storage.
	 * 
	 * @param uuid The {@link Player}'s UUID
	 * @return Their {@link LoginData}, if it exists
	 */
	public LoginData popLoginData(UUID uuid) {
		BukkitTask cleanupTask = cleanupTasks.remove(uuid);
		if (cleanupTask != null) {
			cleanupTask.cancel();
		}

		return loginData.remove(uuid);
	}

	/**
	 * Stores the {@link LoginData} for the given {@link Player}. This will be
	 * automatically deleted after the set delay, such as if the player fails to
	 * complete the login.
	 * 
	 * @param uuid The {@link Player}'s {@link UUID}
	 * @param data Their {@link LoginData}
	 * @return Whether this overwrote existing {@link LoginData}
	 */
	public boolean putLoginData(UUID uuid, LoginData data) {
		BukkitTask previousTask = cleanupTasks.put(uuid, new BukkitRunnable() {
			@Override
			public void run() {
				cleanupTasks.remove(uuid);
				loginData.remove(uuid);
			}
		}.runTaskLaterAsynchronously(StratusAPI.get(), CLEANUP_DELAY));

		if (previousTask != null) {
			previousTask.cancel();
		}

		return loginData.put(uuid, data) != null;
	}

}
