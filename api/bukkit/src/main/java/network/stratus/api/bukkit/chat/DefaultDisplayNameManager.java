/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.chat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import network.stratus.api.bukkit.StratusAPI;

/**
 * The default display name manager, which sets the {@link Player}'s display
 * name to be their prefixes followed by their name in the default online
 * colour.
 * 
 * @author Ian Ballingall
 *
 */
public class DefaultDisplayNameManager implements DisplayNameManager {

	@Override
	public void setDisplayName(Player player) {
		player.setDisplayName(
				StratusAPI.get().getPermissionsManager().getGroupsManager()
						.getPrefixes(player.getUniqueId(), StratusAPI.get().getPermissionsManager().getRealms())
						+ Chat.ONLINE_COLOR_DEFAULT + player.getName() + ChatColor.RESET);
	}

}
