package network.stratus.api.bukkit.afk;

public enum AFKAction {
    MARK,
    KICK;
}
