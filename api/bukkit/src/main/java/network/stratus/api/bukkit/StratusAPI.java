/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.annotation.Nullable;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

import app.ashcon.intake.bukkit.BukkitIntake;
import app.ashcon.intake.bukkit.graph.BasicBukkitCommandGraph;
import app.ashcon.intake.fluent.DispatcherNode;
import co.aikar.taskchain.BukkitTaskChainFactory;
import co.aikar.taskchain.TaskChain;
import co.aikar.taskchain.TaskChainFactory;
import network.stratus.api.bukkit.afk.AFKAction;
import network.stratus.api.bukkit.afk.AFKListener;
import network.stratus.api.bukkit.afk.AFKManager;
import network.stratus.api.bukkit.chat.DefaultDisplayNameManager;
import network.stratus.api.bukkit.chat.DisplayNameManager;
import network.stratus.api.bukkit.chat.MuteManager;
import network.stratus.api.bukkit.commands.*;
import network.stratus.api.bukkit.commands.modules.StratusBukkitCommandModule;
import network.stratus.api.bukkit.listeners.ChatListener;
import network.stratus.api.bukkit.listeners.JoinListener;
import network.stratus.api.bukkit.listeners.LoginListener;
import network.stratus.api.bukkit.listeners.QuitListener;
import network.stratus.api.bukkit.messaging.AdminChatMessageProcessor;
import network.stratus.api.bukkit.messaging.ConsumerExceptionHandler;
import network.stratus.api.bukkit.messaging.GlobalBroadcastProcessor;
import network.stratus.api.bukkit.messaging.PunishmentMessageProcessor;
import network.stratus.api.bukkit.messaging.ReportMessageProcessor;
import network.stratus.api.bukkit.models.BukkitReport;
import network.stratus.api.bukkit.models.punishments.BukkitPunishmentFactory;
import network.stratus.api.bukkit.permissions.BukkitPermissionsManager;
import network.stratus.api.bukkit.server.LoginManager;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.JAXRSClient;
import network.stratus.api.client.RequestFailureException;
import network.stratus.api.i18n.DefaultLocaleTranslator;
import network.stratus.api.i18n.LanguageManager;
import network.stratus.api.i18n.Translator;
import network.stratus.api.messaging.JsonConsumer;
import network.stratus.api.messaging.JsonPublisher;
import network.stratus.api.messaging.Publisher;
import network.stratus.api.messaging.RabbitMessenger;
import network.stratus.api.models.AdminChatMessage;
import network.stratus.api.models.GlobalBroadcast;
import network.stratus.api.models.server.ServerStatus;
import network.stratus.api.permissions.RestGroupsManager;
import network.stratus.api.requests.ServerStatusChangeRequest;
import network.stratus.api.responses.ServerResponse;
import network.stratus.api.server.SessionManager;
import org.bukkit.plugin.messaging.PluginMessageListener;

/**
 * The main class of the core Stratus API plugin. The core plugin may be
 * deployed on any Stratus service where it is required. This provides an API
 * client, task chain system, internationalisation, display name management,
 * permissions system, and punishments and reports systems.
 *
 * @author Ian Ballingall
 *
 */
public class StratusAPI extends JavaPlugin {

	private static StratusAPI plugin;

	private APIClient apiClient;
	private TaskChainFactory taskChainFactory;
	private RabbitMessenger messenger;

	private String serverName;
	private Map<String, Translator> translators = new HashMap<>();

	private DisplayNameManager displayNameManager = new DefaultDisplayNameManager();
	private BukkitPermissionsManager permissionsManager;
	private MuteManager muteManager;
	private LoginManager loginManager;
	private SessionManager sessionManager;

	private Publisher sendChatPublisher;
	private Publisher pgmStatusPublisher;
	private Publisher rankedStatusPublisher;

	public class StratusMessageListener implements PluginMessageListener {

		@Override
		public void onPluginMessageReceived(String channel, Player player, byte[] message) {

		}
	}

	@Override
	public void onEnable() {
		plugin = this;
		saveDefaultConfig();

		// Set up the translator and languages
		try {
			translators.put("default", new DefaultLocaleTranslator(
					new LanguageManager.Builder(Locale.UK)
							.loadExternally("./i18n").loadProperties().build()));
		} catch (IOException e) {
			getLogger().severe("Failed to load language information: " + e);
			e.printStackTrace();
			getServer().getPluginManager().disablePlugin(this);
		}
		this.getServer().getMessenger().registerIncomingPluginChannel(this, "Stratus", new    StratusMessageListener());
		// Set up the API client
		String apiClientUrl = getConfig().getString("api.url");
		String authToken = getConfig().getString("api.authToken");
		apiClient = new JAXRSClient.Builder(apiClientUrl).authToken(authToken).build();

		final ServerResponse serverData;
		try {
			// Notify the API of the server start and obtain relevant data
			serverData = new ServerStatusChangeRequest(ServerStatus.ONLINE).make(apiClient);
		} catch (RequestFailureException e) {
			e.printStackTrace();
			getLogger().severe("Failed to connect to backend, shutting down");
			e.getResponse().ifPresent(response -> getLogger().severe(response.getDescription()));
			getServer().shutdown();
			return;
		}

		serverName = serverData.getName();

		// Set up the RabbitMQ messaging service, if enabled
		if (getConfig().getBoolean("messaging.enabled", false)) {
			messenger = new RabbitMessenger(getConfig().getString("messaging.uri"), new ConsumerExceptionHandler());
			try {
				messenger.initialiseConnection();
				configureMessagingBroadcastListeners();
				configureSendChat();
				configureSendStatus();
			} catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException | IOException
					| TimeoutException | UnrecoverableKeyException | KeyStoreException | CertificateException e) {
				getLogger().severe("Failed to connect to RabbitMQ server: " + e);
				getLogger().severe("Messaging services will be disabled");
				e.printStackTrace();
				messenger = null;
			}
		}

		// Set up the permissions manager
		permissionsManager = new BukkitPermissionsManager(serverData.getRealms(),
				new RestGroupsManager(Collections.singletonList("_default"), apiClient));

		// Set up the mute manager
		List<String> mutedCommands = getConfig().getStringList("mutes.mutedCommands");
		muteManager = new MuteManager(new HashSet<String>(mutedCommands),
				getConfig().getBoolean("mutes.onlineTimeIsGameplay", true),
				getConfig().getLong("mutes.updatePeriod", 60000));

		// Set up the login manager
		loginManager = new LoginManager();

		// Set up the session manager
		sessionManager = new SessionManager();

		// Set up the afk manager
		if (getConfig().getBoolean("afk.enabled", false)) {
			getServer().getPluginManager().registerEvents(new AFKListener(), this);
			AFKManager.get().setEnabled(true);
			AFKManager.get().setWarningSoundEnabled(getConfig().getBoolean("afk.sound", true));
			AFKManager.get().setAFKTime(getConfig().getInt("afk.time", 50));
			AFKManager.get().setGraceTimeSeconds(getConfig().getInt("afk.grace", 10));
			switch (getConfig().getString("afk.action", "mark").toLowerCase()) {
				case "kick": AFKManager.get().setAction(AFKAction.KICK); break;
				default: case "mark": AFKManager.get().setAction(AFKAction.MARK); break;
			}
			getServer().getScheduler().runTaskTimer(this, () -> {
				AFKManager.get().tick();
			}, 20 , 20);
		}

		// Set up event listeners
		getServer().getPluginManager().registerEvents(new LoginListener(), this);
		getServer().getPluginManager().registerEvents(new JoinListener(), this);
		getServer().getPluginManager().registerEvents(new QuitListener(), this);
		getServer().getPluginManager().registerEvents(new ChatListener(), this);

		// Set up commands
		BasicBukkitCommandGraph cmdGraph = new BasicBukkitCommandGraph(new StratusBukkitCommandModule());
		DispatcherNode root = cmdGraph.getRootDispatcherNode();

		root.registerCommands(new PunishmentCommands());
		root.registerCommands(new ReportCommands(getConfig().getLong("cooldowns.reports", 10000)));
		root.registerCommands(new PlayerCommands());

		root.registerNode("permissions").registerCommands(new PermissionCommands());
		root.registerNode("group").registerCommands(new GroupCommands());

		DispatcherNode teams = root.registerNode("teams");
		teams.registerCommands(new TeamCommands(10000));
		teams.registerNode("admin").registerCommands(new TeamAdminCommands());

		configureChatCommands(root);

		new BukkitIntake(this, cmdGraph).register();

		// Register task chain
		taskChainFactory = BukkitTaskChainFactory.create(this);

		getLogger().info("Stratus API enabled");
	}

	@Override
	public void onDisable() {
		plugin = null;

		try {
			new ServerStatusChangeRequest(ServerStatus.OFFLINE).make(apiClient);
		} catch (RequestFailureException e) {
			e.printStackTrace();
			getLogger().warning("Failed to notify backend of server shutdown");
			e.getResponse().ifPresent(response -> getLogger().warning(response.getDescription()));
		}

		if (messenger != null) {
			messenger.closeConnection();
		}

		getLogger().info("Stratus API disabled");
	}

	/**
	 * Get the current Stratus API plugin instance.
	 *
	 * @return The plugin instance object
	 */
	public static StratusAPI get() {
		if (plugin == null)
			throw new IllegalStateException("Plugin is not enabled");

		return plugin;
	}

	/**
	 * Instantiate a new {@link TaskChain} for linking synchronous and asynchronous
	 * tasks together.
	 *
	 * @return A new {@link TaskChain}
	 */
	public <T> TaskChain<T> newChain() {
		return taskChainFactory.newChain();
	}

	/**
	 * Obtain the named {@code SharedTaskChain}. If it does not
	 * exist, a new chain by this name shall be created. Each separate queue of
	 * tasks added to a shared chain will be run sequentially.
	 *
	 * @param name The name of the shared chain
	 * @return The corresponding {@code SharedTaskChain}
	 */
	public <T> TaskChain<T> newSharedChain(String name) {
		return taskChainFactory.newSharedChain(name);
	}

	public APIClient getApiClient() {
		return apiClient;
	}

	public BukkitPermissionsManager getPermissionsManager() {
		return permissionsManager;
	}

	public String getServerName() {
		return serverName;
	}

	public Translator getTranslator() {
		return translators.get("default");
	}

	public Translator getTranslator(String key) {
		return translators.get(key);
	}

	public DisplayNameManager getDisplayNameManager() {
		return displayNameManager;
	}

	public void setDisplayNameManager(DisplayNameManager manager) {
		displayNameManager = manager;
	}

	public MuteManager getMuteManager() {
		return muteManager;
	}

	public LoginManager getLoginManager() {
		return loginManager;
	}

	@Nullable
	public RabbitMessenger getRabbitMessenger() {
		return messenger;
	}

	public SessionManager getSessionManager() {
		return sessionManager;
	}

	public Publisher getSendChatPublisher() {
		return sendChatPublisher;
	}

	public Publisher getPgmStatusPublisher() {
		return pgmStatusPublisher;
	}

	public Publisher getRankedStatusPublisher() {
		return rankedStatusPublisher;
	}

	/**
	 * Sets up the messaging broadcast listeners based on the configuration.
	 *
	 * @throws IOException If registration of a consumer channel fails
	 */
	private void configureMessagingBroadcastListeners() throws IOException {
		// Set up punishments
		if (getConfig().getBoolean("messaging.services.broadcasts.punishments", false)) {
			new JsonConsumer.Builder<BukkitPunishmentFactory>(messenger.getNewChannel(), BukkitPunishmentFactory.class)
					.consumerName("punishments").exchangeName("broadcasts.punishments")
					.exchangeType(BuiltinExchangeType.TOPIC)
					.messageProcessor(new PunishmentMessageProcessor(serverName)).build().register();
		}

		// Set up reports
		if (getConfig().getBoolean("messaging.services.broadcasts.reports", false)) {
			new JsonConsumer.Builder<BukkitReport>(messenger.getNewChannel(), BukkitReport.class)
					.consumerName("reports").exchangeName("broadcasts.reports").exchangeType(BuiltinExchangeType.TOPIC)
					.messageProcessor(new ReportMessageProcessor(serverName)).build().register();
		}
	}

	/**
	 * Configures chat commands, creating messaging publishers and consumers if
	 * enabled, and configuring the commands.
	 *
	 * @param commandNode The node to which the commands will be attached
	 */
	private void configureChatCommands(DispatcherNode commandNode) {
		// Set up admin chat
		Publisher adminChatPublisher = null;
		Publisher broadcastPublisher = null;

		if (messenger != null && getConfig().getBoolean("messaging.services.adminchat.enabled", false)) {
			try {
				String routingKey = getConfig().getString("messaging.services.adminchat.channel");
				new JsonConsumer.Builder<AdminChatMessage>(messenger.getNewChannel(), AdminChatMessage.class)
						.consumerName("adminchat").exchangeName("adminchat").exchangeType(BuiltinExchangeType.TOPIC)
						.routingKey(routingKey).messageProcessor(new AdminChatMessageProcessor(serverName)).build()
						.register();

				Channel publisherChannel = messenger.getNewChannel();
				publisherChannel.exchangeDeclare("adminchat", BuiltinExchangeType.TOPIC);
				adminChatPublisher = new JsonPublisher(publisherChannel, "adminchat", routingKey,
						new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("UTF-8")
								.deliveryMode(1).build());
			} catch (IOException e) {
				getLogger().severe("Failed to configure admin chat: " + e);
				e.printStackTrace();
			}
		}

		if (messenger != null && getConfig().getBoolean("messaging.services.broadcasts.globalBroadcasts", false)) {
			try {
				new JsonConsumer.Builder<GlobalBroadcast>(messenger.getNewChannel(), GlobalBroadcast.class)
						.consumerName("globalBroadcasts").exchangeName("broadcasts.globalBroadcasts")
						.exchangeType(BuiltinExchangeType.TOPIC)
						.messageProcessor(new GlobalBroadcastProcessor(serverName)).build().register();

				Channel publisherChannel = messenger.getNewChannel();
				publisherChannel.exchangeDeclare("broadcasts.globalBroadcasts", BuiltinExchangeType.TOPIC);
				broadcastPublisher = new JsonPublisher(publisherChannel, "broadcasts.globalBroadcasts", "json",
						new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("UTF-8")
								.deliveryMode(1).build());
			} catch (IOException e) {
				getLogger().severe("Failed to configure global broadcasts: " + e);
				e.printStackTrace();
			}
		}

		commandNode.registerCommands(new ChatCommands(adminChatPublisher, broadcastPublisher));
	}

	public void addTranslator(String key, Translator translator) {
		translators.put(key, translator);
	}

	private void configureSendChat() throws IOException {
		if (messenger != null && getConfig().getBoolean("messaging.services.sendchat.enabled", false)) {
			Channel publisherChannel = messenger.getNewChannel();
			publisherChannel.exchangeDeclare("proxy.sendchat", BuiltinExchangeType.TOPIC);
			sendChatPublisher = new JsonPublisher(publisherChannel, "proxy.sendchat", "json",
					new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("UTF-8")
							.deliveryMode(1).build());
		}
	}

	private void configureSendStatus() throws IOException {
		if (messenger != null) {
			Channel pgmPublisherChannel = messenger.getNewChannel();
			pgmPublisherChannel.exchangeDeclare("status.pgmstate", BuiltinExchangeType.TOPIC);
			pgmStatusPublisher = new JsonPublisher(pgmPublisherChannel, "status.pgmstate", "json",
					new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("UTF-8")
							.deliveryMode(1).build());

			Channel rankedPublisherChannel = messenger.getNewChannel();
			rankedPublisherChannel.exchangeDeclare("status.rankedstate", BuiltinExchangeType.TOPIC);
			rankedStatusPublisher = new JsonPublisher(rankedPublisherChannel, "status.rankedstate", "json",
					new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("UTF-8")
							.deliveryMode(1).build());
		}
	}

}
