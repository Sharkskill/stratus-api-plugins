/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.listeners;

import java.util.Date;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.MuteManager;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.models.punishment.TimedPunishment;

/**
 * Listens on chat events to perform require chat management functions,
 * such as enforcing active mutes.
 *
 * @author Ian Ballingall
 *
 */
public class ChatListener implements Listener {

	@EventHandler(priority = EventPriority.NORMAL)
	public void onChat(AsyncPlayerChatEvent event) {
		TimedPunishment mute = StratusAPI.get().getMuteManager().getActiveMute(event.getPlayer().getUniqueId());
		if (mute != null) {
			Date now = new Date();
			SingleAudience audience = new SingleAudience(event.getPlayer());
			audience.sendMessage("punishment.mute.message.chatattempt");
			audience.sendMessage("punishment.mute.message.2", mute.getReason());
			audience.sendMessage("punishment.mute.message.3", StratusAPI.get().getTranslator().getTimeDifferenceString(event.getPlayer().getLocale(), new Date(now.getTime() + mute.getTimeRemaining()), now));
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onCommand(PlayerCommandPreprocessEvent event) {
		MuteManager manager = StratusAPI.get().getMuteManager();
		int argumentsStart = event.getMessage().indexOf(' ');
		String command = (argumentsStart == -1) ? event.getMessage().toLowerCase() : event.getMessage().substring(0, argumentsStart).toLowerCase();

		if (manager.isCommandMuted(command)) {
			TimedPunishment mute = manager.getActiveMute(event.getPlayer().getUniqueId());
			if (mute != null) {
				Date now = new Date();
				SingleAudience audience = new SingleAudience(event.getPlayer());
				audience.sendMessage("punishment.mute.message.commandattempt");
				audience.sendMessage("punishment.mute.message.2", mute.getReason());
				audience.sendMessage("punishment.mute.message.3", StratusAPI.get().getTranslator().getTimeDifferenceString(event.getPlayer().getLocale(), new Date(now.getTime() + mute.getTimeRemaining()), now));
				event.setCancelled(true);
			}
		}
	}

}
