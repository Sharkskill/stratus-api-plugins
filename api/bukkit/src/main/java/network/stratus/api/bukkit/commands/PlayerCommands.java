/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import java.util.Date;

import org.bukkit.command.CommandSender;
import app.ashcon.intake.Command;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.models.UserData;
import network.stratus.api.bukkit.requests.NameUserDataRequest;
import network.stratus.api.bukkit.requests.UserDataRequest;
import network.stratus.api.bukkit.util.Util;

/**
 * Commands pertaining directly to players.
 * 
 * @author Ian Ballingall
 *
 */
public class PlayerCommands {

	@Command(aliases = { "find", "seen" },
			desc = "Find where a player currently is or where they were last seen",
			usage = "<player>")
	public void onFind(CommandSender sender, TabCompletePlayer target) {
		SingleAudience audience = new SingleAudience(sender);

		if (target.getPlayer().isPresent()) {
			audience.sendMessage("player.find.online", target.getPlayer().get().getDisplayName());
		} else {
			UserDataRequest request = new NameUserDataRequest.Builder(target.getUsername()).location().build();
			StratusAPI.get().newChain().<UserData>asyncFirst(() -> {
				return request.make(StratusAPI.get().getApiClient());
			}).syncLast(response -> {
				UserData.Location location = response.getLocation();
				if (location.getOnlineStatus()) {
					audience.sendMessage("player.find.online.otherserver", Chat.OFFLINE_COLOR + response.getUsername(),
							location.getServerName());
				} else {
					audience.sendMessage("player.find.offline", Chat.OFFLINE_COLOR + response.getUsername(),
							StratusAPI.get().getTranslator().getTimeDifferenceString(sender.getLocale(),
									location.getTime(), new Date()),
							location.getServerName());
				}
			}).execute((e, task) -> Util.handleCommandException(e, task, sender));
		}
	}

}
