/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.messaging;

import network.stratus.api.bukkit.chat.Chat;
import org.bukkit.scheduler.BukkitRunnable;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.BukkitReport;
import network.stratus.api.messaging.MessageProcessor;

/**
 * Processes a message which contains a {@link BukkitReport} by broadcasting it.
 * 
 * @author Ian Ballingall
 *
 */
public class ReportMessageProcessor implements MessageProcessor<BukkitReport> {

	private String serverName;

	public ReportMessageProcessor(String serverName) {
		this.serverName = serverName;
	}

	@Override
	public void process(BukkitReport report) {
		if (!report.getServerName().equals(serverName)) {
			StratusAPI plugin = StratusAPI.get();
			new BukkitRunnable() {
				@Override
				public void run() {
					report.broadcast(true);
				}
			}.runTask(plugin);

			SingleAudience.CONSOLE.sendMessage("report.broadcast.server", report.getServerName(),
					Chat.getDisplayName(report.getReporter()), Chat.getDisplayName(report.getTarget()), report.getReason());
		}
	}

}
