package network.stratus.api.bukkit.afk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class AFKManager {

    private static final AFKManager instance = new AFKManager();
    public static AFKManager get() { return instance; }

    private long afkTimeSeconds = 50;
    private long graceTimeSeconds = 10;
    private AFKAction action = AFKAction.MARK;
    private boolean warningSoundEnabled = true;
    private boolean enabled = false;

    private Map<UUID, Long> lastActivity = new HashMap<>();
    private Set<UUID> warned = new HashSet<>();
    private Set<UUID> marked = new HashSet<>();
    private List<PlayerFilter> filters = new ArrayList<>();

    public void updateActivity(UUID uuid) {
        lastActivity.put(uuid, System.currentTimeMillis());
        if (isWarned(uuid)) {
            Player player = Bukkit.getPlayer(uuid);
            if (player != null) {
                player.sendMessage(ChatColor.GOLD + "You are no longer AFK.");
            }
        }
        warned.remove(uuid);
        marked.remove(uuid);
    }

    /**
     * This is used on exempt players (e.g. in between matches, observers)
     * so that they aren't marked as AFK.
     */
    public void resetTime(UUID uuid) {
        lastActivity.put(uuid, System.currentTimeMillis());
    }

    public void clearActivity(UUID uuid) {
        lastActivity.remove(uuid);
        warned.remove(uuid);
        marked.remove(uuid);
    }

    public boolean isAFK(UUID uuid) {
        return lastActivity.containsKey(uuid) && System.currentTimeMillis() - lastActivity.get(uuid) >= 1000 * (afkTimeSeconds + graceTimeSeconds);
    }

    public boolean isInGraceTime(UUID uuid) {
        return lastActivity.containsKey(uuid) && System.currentTimeMillis() - lastActivity.get(uuid) >= 1000 * afkTimeSeconds;
    }

    public boolean isWarned(UUID uuid) {
        return warned.contains(uuid);
    }

    public boolean isMarked(UUID uuid) {
        return marked.contains(uuid);
    }

    public boolean isPlayerAFKable(Player player) {
        if (!enabled) {
            return false;
        }

        for (PlayerFilter filter : filters) {
            if (!filter.isPlayerAFKable(player)) {
                return false;
            }
        }
        return true;
    }

    private void warn(UUID uuid) {
        Player player = Bukkit.getPlayer(uuid);
        if (player != null) {
            warned.add(uuid);
            player.sendMessage(ChatColor.GOLD + "You will be AFK in " + graceTimeSeconds + " seconds.");
            if (warningSoundEnabled) {
                player.playSound(player.getLocation(), Sound.ANVIL_LAND, 1, 1);
            }
        }
    }

    private void mark(UUID uuid) {
        Player player = Bukkit.getPlayer(uuid);
        if (player != null) {
            warned.add(uuid);
            marked.add(uuid);
            switch (action) {
                case KICK:
                    player.kickPlayer(ChatColor.RED + "You were AFK for too long.");
                    break;
                case MARK:
                    player.sendMessage(ChatColor.GOLD + "You are now AFK.");
                    break;
            }
        }
    }

    public void tick() {
        if (!enabled) return;
        for (UUID uuid : lastActivity.keySet()) {
            if (!isPlayerAFKable(Bukkit.getPlayer(uuid))) {
                resetTime(uuid);
            } else if (!isMarked(uuid) && isAFK(uuid)) {
                mark(uuid);
            } else if (!isWarned(uuid) && isInGraceTime(uuid)) {
                warn(uuid);
            }
        }
    }

    public long getAFKTime() {
        return afkTimeSeconds;
    }

    public void setAFKTime(long afkTime) {
        this.afkTimeSeconds = afkTime;
    }

    public long getGraceTimeSeconds() {
        return graceTimeSeconds;
    }

    public void setGraceTimeSeconds(long graceTime) {
        this.graceTimeSeconds = graceTime;
    }

    public AFKAction getAction() {
        return action;
    }

    public void setAction(AFKAction action) {
        this.action = action;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isWarningSoundEnabled() {
        return warningSoundEnabled;
    }

    public void setWarningSoundEnabled(boolean warningSoundEnabled) {
        this.warningSoundEnabled = warningSoundEnabled;
    }

    public void addPlayerFilter(PlayerFilter filter) {
        filters.add(filter);
    }

    public interface PlayerFilter {
        boolean isPlayerAFKable(Player player);
    }
}
