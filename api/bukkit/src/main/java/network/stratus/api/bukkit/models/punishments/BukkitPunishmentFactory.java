/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models.punishments;

import network.stratus.api.models.punishment.Punishment;
import network.stratus.api.models.punishment.PunishmentFactory;

/**
 * Constructs the relevant type of {@link BukkitPunishment} or
 * {@link TimedBukkitPunishment} based on the factory's fields.
 * 
 * @author Ian Ballingall
 *
 */
public class BukkitPunishmentFactory extends PunishmentFactory {

	@Override
	public Punishment getObject() {
		switch (type) {
		case WARN:
			return new Warning(_id, issuer, target, time, expiry, reason, active, number, serverName);
		case KICK:
			return new Kick(_id, issuer, target, time, expiry, reason, active, number, serverName, silent);
		case BAN:
			return new Ban(_id, issuer, target, time, expiry, reason, active, number, serverName, silent);
		case MUTE:
			return new Mute(_id, issuer, target, time, expiry, reason, active, number, serverName, timeRemaining);
		default:
			throw new IllegalArgumentException("Type cannot be instantiated: " + type.toString());
		}
	}

}
