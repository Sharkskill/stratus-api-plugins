/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.permissions;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.permissions.Group;
import network.stratus.api.permissions.GroupsManager;
import network.stratus.api.permissions.PermissionsManager;

/**
 * Manages the permissions on behalf of this plugin.
 * 
 * @author Ian Ballingall
 *
 */
public class BukkitPermissionsManager implements PermissionsManager {

	private GroupsManager groupsManager;
	/** Maps a player's UUID to their {@link PermissionAttachment} object. */
	private Map<UUID, PermissionAttachment> permissionsMap;
	/** The list of permissions realms this server subscribes to. */
	private List<String> realms;

	public BukkitPermissionsManager(List<String> realms, GroupsManager groupsManager) {
		this.groupsManager = groupsManager;
		this.permissionsMap = new ConcurrentHashMap<>();
		this.realms = realms;
		this.realms.add(0, "global"); // Add the default realm
	}

	@Override
	public GroupsManager getGroupsManager() {
		return groupsManager;
	}

	public List<String> getRealms() {
		return realms;
	}

	/**
	 * Sets the {@link Player}'s permissions given a list of nodes.
	 * 
	 * @param player      The {@link Player} whose permissions will be set
	 * @param permissions The {@link List} of permission nodes
	 */
	public void setPermissions(Player player, List<String> permissions) {
		PermissionAttachment attachment = getPermissionAttachment(player);

		for (String permission : permissions) {
			setPermission(attachment, permission);
		}
	}

	/**
	 * Sets the {@link Player}'s permissions given a list of nodes.
	 * 
	 * @param uuid        The {@link UUID} of the {@link Player} whose permissions
	 *                    will be set
	 * @param permissions The {@link List} of permission nodes
	 */
	@Override
	public void setPermissions(UUID uuid, List<String> permissions) {
		setPermissions(StratusAPI.get().getServer().getPlayer(uuid), permissions);
	}

	/**
	 * Sets the {@link Player}'s permissions given a permissions {@link Group}.
	 * 
	 * @param player The {@link Player} whose permissions will be set
	 * @param group  The {@link Group} whose permissions will be added to the player
	 */
	public void setPermissions(Player player, Group group) {
		setPermissions(player, group.getPermissions(realms));
		groupsManager.addPlayerToGroup(player.getUniqueId(), group);
	}

	/**
	 * Sets the {@link Player}'s permissions given a permissions {@link Group}.
	 * 
	 * @param uuid  The {@link UUID} of the {@link Player} whose permissions will be
	 *              set
	 * @param group The {@link Group} whose permissions will be added to the player
	 */
	@Override
	public void setPermissions(UUID uuid, Group group) {
		setPermissions(StratusAPI.get().getServer().getPlayer(uuid), group);
	}

	/**
	 * Sets a single permission on the given {@link Player}.
	 * 
	 * @param player     The {@link Player} whose permissions will be set
	 * @param permission The permission node to set
	 */
	public void setPermission(Player player, String permission) {
		setPermission(getPermissionAttachment(player), permission);
	}

	/**
	 * Sets a single permission on the given {@link Player}.
	 * 
	 * @param uuid       The {@link UUID} of the player whose permissions will be
	 *                   set
	 * @param permission The permission node to set
	 */
	@Override
	public void setPermission(UUID uuid, String permission) {
		setPermission(StratusAPI.get().getServer().getPlayer(uuid), permission);
	}

	/**
	 * Set a single permission on the given {@link Player} with the specified value.
	 * 
	 * @param player     The {@link Player} whose permissions will be set
	 * @param permission The permission node to set
	 * @param value      Whether to grant or negate this permission
	 */
	public void setPermission(Player player, String permission, boolean value) {
		setPermission(getPermissionAttachment(player), permission, value);
	}

	/**
	 * Set a single permission on the given {@link Player} with the specified value.
	 * 
	 * @param uuid       The {@link UUID} of the player whose permissions will be
	 *                   set
	 * @param permission The permission node to set
	 * @param value      Whether to grant or negate this permission
	 */
	@Override
	public void setPermission(UUID uuid, String permission, boolean value) {
		setPermission(StratusAPI.get().getServer().getPlayer(uuid), permission, value);
	}

	/**
	 * Unset the given list of permissions from a {@link Player}. This does not
	 * negate the permission nodes, but rather means this plugin will no longer
	 * manage this set of permission nodes.
	 * 
	 * @param player      The {@link Player} whose permissions will be unset
	 * @param permissions The {@link List} of permission nodes
	 */
	public void unsetPermissions(Player player, List<String> permissions) {
		PermissionAttachment attachment = getPermissionAttachment(player);

		for (String permission : permissions) {
			unsetPermission(attachment, permission);
		}
	}

	/**
	 * Unset the given list of permissions from a {@link Player}. This does not
	 * negate the permission nodes, but rather means this plugin will no longer
	 * manage this set of permission nodes.
	 * 
	 * @param uuid        The {@link UUID} of the player whose permissions will be
	 *                    unset
	 * @param permissions The list of permission nodes
	 */
	@Override
	public void unsetPermissions(UUID uuid, List<String> permissions) {
		unsetPermissions(StratusAPI.get().getServer().getPlayer(uuid), permissions);
	}

	/**
	 * Unset a single permission node from a {@link Player}. This does not negate
	 * the permission node, but rather means this plugin will no longer manage this
	 * permission node.
	 * 
	 * @param player     player The {@link Player} whose permissions will be unset
	 * @param permission The permission node to unset
	 */
	public void unsetPermission(Player player, String permission) {
		unsetPermission(getPermissionAttachment(player), permission);
	}

	/**
	 * Unset a single permission node from a {@link Player}. This does not negate
	 * the permission node, but rather means this plugin will no longer manage this
	 * permission node.
	 * 
	 * @param uuid       The {@link UUID} of the player whose permissions will be
	 *                   unset
	 * @param permission The permission node to unset
	 */
	@Override
	public void unsetPermission(UUID uuid, String permission) {
		unsetPermission(StratusAPI.get().getServer().getPlayer(uuid), permission);
	}

	/**
	 * Detach the given {@link Player} from the permissions manager. This means the
	 * plugin will stop managing this player's permissions. This method <b>must</b>
	 * be called when the player disconnects.
	 * 
	 * @param player The {@link Player} who will be detached
	 */
	public void detachPermissions(Player player) {
		PermissionAttachment attachment = permissionsMap.remove(player.getUniqueId());
		if (attachment != null) {
			try {
				player.removeAttachment(attachment);
			} catch (IllegalArgumentException e) {
				StratusAPI.get().getLogger().severe("Failed to remove permission attachment for " + player.getName());
				e.printStackTrace();
			}
		}

		groupsManager.removePlayer(player.getUniqueId());
	}

	/**
	 * Detach the {@link Player} player from the permissions manager. This means the
	 * plugin will stop managing this player's permissions. <b>This method must be
	 * called when the player disconnects.</b>
	 * 
	 * @param uuid The {@link UUID} of the player who will be detached
	 */
	@Override
	public void detachPermissions(UUID uuid) {
		detachPermissions(StratusAPI.get().getServer().getPlayer(uuid));
	}

	/**
	 * Applies the given permission node to a {@link Player} via their
	 * {@link PermissionAttachment}.
	 * 
	 * @param attachment The {@link Player}'s {@link PermissionAttachment}
	 * @param permission The permission node, which will be negated if it starts
	 *                   with a -
	 */
	private void setPermission(PermissionAttachment attachment, String permission) {
		boolean value = true;
		if (permission.charAt(0) == '-') {
			value = false;
			permission = permission.substring(1);
		}

		setPermission(attachment, permission, value);
	}

	/**
	 * Applies the given permission node to a {@link Player} via their
	 * {@link PermissionAttachment}.
	 * 
	 * @param attachment The {@link Player}'s {@link PermissionAttachment}
	 * @param permission The permission node, without any negation markings
	 * @param value      Whether to grant or negate this permission
	 */
	private void setPermission(PermissionAttachment attachment, String permission, boolean value) {
		attachment.setPermission(permission, value);
	}

	/**
	 * Unsets a permission given a {@link PermissionAttachment}.
	 * 
	 * @param attachment The {@link Player}'s {@link PermissionAttachment}
	 * @param permission The permission node, without any negation markings
	 */
	private void unsetPermission(PermissionAttachment attachment, String permission) {
		attachment.unsetPermission(permission);
	}

	/**
	 * Obtain the {@link Player}'s {@link PermissionAttachment}, or create it if it
	 * doesn't exist.
	 * 
	 * @param player The {@link Player} whose {@link PermissionAttachment} will be
	 *               obtained, or who will be attached
	 * @return The permission attachment for this player
	 */
	private PermissionAttachment getPermissionAttachment(Player player) {
		PermissionAttachment attachment = permissionsMap.get(player.getUniqueId());
		if (attachment == null) {
			attachment = player.addAttachment(StratusAPI.get());
			permissionsMap.put(player.getUniqueId(), attachment);
		}

		return attachment;
	}

}
