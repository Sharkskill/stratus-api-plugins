/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import org.bukkit.command.CommandSender;
import app.ashcon.intake.Command;
import app.ashcon.intake.parametric.annotation.Switch;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.requests.PermissionsRefreshRequest;

/**
 * Represents the commands associated with the permissions system.
 * 
 * @author Ian Ballingall
 *
 */
public class PermissionCommands {

	@Command(aliases = "refresh",
			desc = "Reloads the permissions files",
			perms = "stratusapi.command.permissions.refresh")
	public void refresh(CommandSender sender, @Switch('g') boolean globalReload) {
		StratusAPI.get().newSharedChain("groups").async(() -> {
			if (globalReload)
				new PermissionsRefreshRequest().make(StratusAPI.get().getApiClient());
		}).sync(() -> {
			SingleAudience audience = new SingleAudience(sender);

			audience.sendMessage("permissions.refresh.success");
			audience.sendMessage("permissions.refresh.updatewarning");

			StratusAPI.get().getPermissionsManager().getGroupsManager().clearGroups();
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

}
