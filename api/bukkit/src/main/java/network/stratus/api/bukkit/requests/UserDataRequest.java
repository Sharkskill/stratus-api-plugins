/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

import network.stratus.api.bukkit.models.UserData;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.models.punishment.PunishmentFactory;
import network.stratus.api.models.punishment.PunishmentFactory.Type;
import network.stratus.api.util.Util;

/**
 * Represents a request to obtain the {@link UserData} for a player.
 * 
 * @author Ian Ballingall
 *
 */
public abstract class UserDataRequest implements Request<UserData> {

	@JsonIgnore
	private List<String> data;
	@JsonIgnore
	private List<PunishmentFactory.Type> punishmentTypes;

	protected UserDataRequest(List<String> data, List<Type> punishmentTypes) {
		super();
		this.data = data;
		this.punishmentTypes = punishmentTypes;
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> properties = new HashMap<>();

		if (!data.isEmpty()) {
			properties.put("data", Util.buildPropertiesListString(data));
		}

		if (!punishmentTypes.isEmpty()) {
			properties.put("punishmentTypes", Util.buildPropertiesListString(punishmentTypes));
		}

		if (data.contains("team")) {
			// Only support deep populate, as Team requires Users
			properties.put("populateTeam", "deep");
		}

		if (data.contains("groups")) {
			// Only support populate, as we want full group data
			properties.put("populateGroups", true);
		}

		return properties;
	}

	@Override
	public UserData make(APIClient client) {
		return client.get(this);
	}

	@Override
	public Class<UserData> getResponseType() {
		return UserData.class;
	}

	/**
	 * Builds a new {@link UserDataRequest}.
	 */
	public abstract static class Builder {

		protected boolean allData = false;
		protected List<String> data = new ArrayList<>();
		protected List<PunishmentFactory.Type> punishmentTypes = new ArrayList<>();

		/**
		 * Obtain all available data.
		 * 
		 * @return The builder
		 */
		public Builder allData() {
			allData = true;
			data.clear();
			return this;
		}

		/**
		 * Obtain the player's PGM statistics.
		 * 
		 * @return The builder
		 */
		public Builder pgmStatistics() {
			if (!allData)
				data.add("pgmStatistics");
			return this;
		}

		/**
		 * Obtain the player's current or last known location on the network.
		 * 
		 * @return The builder
		 */
		public Builder location() {
			if (!allData)
				data.add("location");
			return this;
		}

		/**
		 * Obtain the player's username history.
		 * 
		 * @return The builder
		 */
		public Builder usernameHistory() {
			if (!allData)
				data.add("usernameHistory");
			return this;
		}

		/**
		 * Obtain the user's punishments data.
		 * 
		 * @return The builder
		 */
		public Builder punishments() {
			if (!allData)
				data.add("punishments");
			return this;
		}

		/**
		 * Obtain the player's team. This will deep populate the team, retrieving all
		 * members' {@link network.stratus.api.models.User} objects.
		 * 
		 * @return The builder
		 */
		public Builder team() {
			if (!allData)
				data.add("team");
			return this;
		}

		/**
		 * Obtain the player's groups. This will populate the groups, retrieving all
		 * members' {@link network.stratus.api.permissions.Group} objects.
		 * 
		 * @return The builder
		 */
		public Builder groups() {
			if (!allData)
				data.add("groups");
			return this;
		}

		/**
		 * Add a type of punishment to retrieve, if retrieving the player's punishments.
		 * 
		 * @param type The type of punishment
		 * @return The builder
		 */
		public Builder punishmentType(PunishmentFactory.Type type) {
			if (type == PunishmentFactory.Type.AUTO)
				throw new IllegalArgumentException("Invalid type specified: AUTO");

			punishmentTypes.add(type);
			return this;
		}

		/**
		 * Obtain the {@link UserDataRequest} for the given parameters.
		 * 
		 * @return The request
		 */
		public abstract UserDataRequest build();

	}

}
