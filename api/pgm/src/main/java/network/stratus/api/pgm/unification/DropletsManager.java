package network.stratus.api.pgm.unification;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.LoginData;
import network.stratus.api.permissions.Group;
import network.stratus.api.pgm.StratusAPIPGM;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import tc.oc.pgm.util.nms.NMSHacks;

import java.time.Duration;
import java.util.UUID;

public class DropletsManager {

    private Droplets droplets;

    public DropletsManager() {

    }

    public Droplets getDroplets() {
        return droplets;
    }

    public void load(Droplets droplets) {
        this.droplets = droplets;
    }

    public double getDropletMultiplier(UUID player) {
        double multiplier = 1;

        LoginData loginData = StratusAPI.get().getLoginManager().getLoginData(player);
        if (loginData == null) {
            return multiplier;
        }

        for (Group group : loginData.getGroups()) {
            multiplier = Math.max(multiplier, droplets.getMultipliers().getOrDefault(group.getName().toLowerCase(), multiplier));
        }

        return multiplier;
    }


    public void playEffect(double droplets, UUID uuid, String translation) {
        playEffect(droplets, StratusAPIPGM.get().getServer().getPlayer(uuid), translation);
    }

    public void playEffect(double droplets, Player player, String translation) {
        if (player == null || !player.isOnline() || droplets <= 0) {
            return;
        }

        String setting = StratusAPIPGM.get().getSettingManager().getSetting(player.getUniqueId(), "droplets");
        if (setting.equalsIgnoreCase("off")) {
            return;
        }

        int count = (int) (droplets * getDropletMultiplier(player.getUniqueId()));

        String dropletsName = (count != 1 && count != -1) ?
                StratusAPI.get().getTranslator().getStringOrDefaultLocale(player.getLocale(), "droplets.plural") :
                StratusAPI.get().getTranslator().getStringOrDefaultLocale(player.getLocale(), "droplets.singular");
        new SingleAudience(player).sendMessage(translation, count, dropletsName);
        player.playSound(player.getLocation(), Sound.LEVEL_UP, 1f, 1.5f);

        NMSHacks.showFakeItems(StratusAPIPGM.get(),
                player,
                player.getLocation().clone().add(0, 2, 0),
                new ItemStack(Material.GHAST_TEAR),
                Math.min(15, count),
                Duration.ofSeconds(3));
    }
}
