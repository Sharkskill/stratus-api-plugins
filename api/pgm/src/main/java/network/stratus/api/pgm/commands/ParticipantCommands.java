/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.models.UserData;
import network.stratus.api.bukkit.requests.NameUserDataRequest;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.match.ParticipantManager;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.teams.TeamMatchModule;

/**
 * Commands for managing a player's participation in a match.
 * 
 * @author Ian Ballingall
 *
 */
public class ParticipantCommands {

	@Command(aliases = "register",
			desc = "Register a player to a team",
			usage = "<player> <team>",
			perms = "stratusapi.command.participant.register")
	public void onRegisterParticipant(CommandSender sender, TabCompletePlayer input, @Text String team) {
		Player player = input.getPlayer().orElse(null);

		StratusAPI.get().newSharedChain("participation").<UserData>asyncFirst(() -> {
			if (player == null) {
				return new NameUserDataRequest.Builder(input.getUsername()).build()
						.make(StratusAPI.get().getApiClient());
			} else {
				return null;
			}
		}).<UUID>sync(response -> {
			if (response == null) {
				return player.getUniqueId();
			} else {
				return response.get_id();
			}
		}).abortIfNull().syncLast(uuid -> {
			ParticipantManager manager = StratusAPIPGM.get().getParticipantManager();
			SingleAudience audience = new SingleAudience(sender);
			if (manager.isPlayerRegistered(PGM.get().getMatchManager().getMatch(sender), uuid)) {
				audience.sendMessage("team.register.failure.alreadyregistered");
				return;
			}

			Match match = PGM.get().getMatchManager().getMatch(sender);
			TeamMatchModule tmm = match.getModule(TeamMatchModule.class);

			if (team.trim().toLowerCase().startsWith("obs")) {
				audience.sendMessage("team.register.failure.obs");
				return;
			} else {
				Party party = tmm.bestFuzzyMatch(team.trim());
				if (party == null) {
					audience.sendMessage("team.register.failure.doesnotexist");
				} else {
					StratusAPIPGM.get().getParticipantManager().registerParticipant(match, uuid, party);
					audience.sendMessage("team.register.success", party.getColor() + party.getNameLegacy());

					if (player != null) {
						MatchPlayer matchPlayer = match.getPlayer((Player) player);
						match.setParty(matchPlayer, party);
					}
				}
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "unregister",
			desc = "Unregisters a player from a team",
			usage = "<player>",
			perms = "stratusapi.command.participant.unregister")
	public void onUnregisterParticipant(CommandSender sender, TabCompletePlayer input) {
		Player player = input.getPlayer().orElse(null);

		StratusAPI.get().newSharedChain("participation").<UserData>asyncFirst(() -> {
			if (player == null) {
				return new NameUserDataRequest.Builder(input.getUsername()).build()
						.make(StratusAPI.get().getApiClient());
			} else {
				return null;
			}
		}).<UUID>sync(response -> {
			if (response == null) {
				return player.getUniqueId();
			} else {
				return response.get_id();
			}
		}).abortIfNull().syncLast(uuid -> {
			SingleAudience audience = new SingleAudience(sender);
			Match match = PGM.get().getMatchManager().getMatch(sender);
			ParticipantManager manager = StratusAPIPGM.get().getParticipantManager();

			if (manager.isPlayerRegistered(match, uuid)) {
				manager.unregisterParticipant(match, uuid);
				audience.sendMessage("team.unregister.success");

				if (player != null) {
					MatchPlayer matchPlayer = match.getPlayer((Player) player);
					match.setParty(matchPlayer, match.getDefaultParty());
				}
			} else {
				audience.sendMessage("team.unregister.failure.notregistered");
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "list",
			desc = "See all registered participants",
			perms = "stratusapi.command.participant.unregister")
	public void onListParticipants(CommandSender sender) {
		SingleAudience audience = new SingleAudience(sender);
		ParticipantManager manager = StratusAPIPGM.get().getParticipantManager();
		Map<UUID, Party> participants = manager.getParticipants(PGM.get().getMatchManager().getMatch(sender));
		if (participants.isEmpty()) {
			audience.sendMessage("participant.list.empty");
			return;
		}

		Map<Party, List<UUID>> teams = new HashMap<>();
		participants.forEach((uuid, party) -> {
			if (!teams.containsKey(party)) {
				teams.put(party, new ArrayList<>());
			}
			
			teams.get(party).add(uuid);
		});

		teams.forEach((party, uuids) -> {
			audience.sendMessageRaw(party.getColor() + party.getNameLegacy() + ":");
			uuids.forEach(uuid -> {
				OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
				final String name;
				if (player.getPlayer() == null) {
					if (player.getName() == null) {
						name = Chat.OFFLINE_COLOR + uuid.toString();
					} else {
						name = Chat.OFFLINE_COLOR + player.getName();
					}
				} else {
					name = player.getPlayer().getDisplayName();
				}

				audience.sendMessageRaw("- " + name);
			});
		});
	}

}
