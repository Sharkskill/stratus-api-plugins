/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.match;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import network.stratus.api.pgm.StratusAPIPGM;
import tc.oc.pgm.api.match.event.MatchLoadEvent;
import tc.oc.pgm.api.match.event.MatchUnloadEvent;
import tc.oc.pgm.api.player.event.MatchPlayerAddEvent;
import tc.oc.pgm.events.PlayerParticipationStartEvent;
import tc.oc.pgm.events.PlayerParticipationStopEvent;

/**
 * Listens on events which occur during a match that are pertinent to managing
 * participants.
 * 
 * @author Ian Ballingall
 *
 */
public interface MatchListener extends Listener {

	/**
	 * Called when a new match is loaded on to the server.
	 * 
	 * @param event The event
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	default public void onMatchLoad(MatchLoadEvent event) {
		StratusAPIPGM.get().getParticipantManager().newMatch(event.getMatch());
		StratusAPIPGM.get().getReadyManager().newMatch(event.getMatch());
	}

	/**
	 * Called when a match finishes.
	 * 
	 * @param event The event
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	default public void onMatchUnload(MatchUnloadEvent event) {
		StratusAPIPGM.get().getParticipantManager().endMatch(event.getMatch());
		StratusAPIPGM.get().getReadyManager().endMatch(event.getMatch());
	}

	/**
	 * Called when a player joins a participating team.
	 * 
	 * @param event The event
	 */
	public void onPlayerJoinTeam(PlayerParticipationStartEvent event);

	/**
	 * Called when a player leaves a participating team.
	 * 
	 * @param event The event
	 */
	public void onPlayerLeaveTeam(PlayerParticipationStopEvent event);

	/**
	 * Called when a player enters a match. This can be used to change the player's
	 * initial team.
	 * 
	 * @param event The event
	 */
	public void onPlayerEnterMatch(MatchPlayerAddEvent event);

}
