/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.teams;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.bukkit.ChatColor;

import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.teams.TeamMatchModule;

/**
 * Strategy which selects a {@link Party}, preferring the colour that the
 * {@link Team} previously had. If it fails, it resorts to
 * {@link DefaultPartySelectStrategy}. The colours are retained indefinitely,
 * even if {@link this#cleanup(Match)} is called. If they are no longer required, then it
 * is recommended to release them using {@link this#resetColours()} or
 * {@link this#resetColour(Team)}..
 * 
 * @author Ian Ballingall
 *
 */
public class PreferredColourPartySelectStrategy extends DefaultPartySelectStrategy {

	/** Maps teams to their preferred colours. */
	protected Map<Team, ChatColor> teamColours;

	public PreferredColourPartySelectStrategy() {
		super();
		this.teamColours = new HashMap<>();
	}

	@Override
	public Party select(Match match, Team team) {
		if (matchTeamParties.containsKey(match) && matchTeamParties.get(match).containsKey(team)) {
			return matchTeamParties.get(match).get(team);
		}

		return selectPartyForTeam(match, team);
	}

	protected Party selectPartyForTeam(Match match, Team team) {
		if (teamColours.containsKey(team)) {
			Optional<tc.oc.pgm.teams.Team> party = match.needModule(TeamMatchModule.class).getTeams().stream()
					.filter(p -> p.getColor().equals(teamColours.get(team))).findAny();
			if (party.isPresent() && !partyTeams.containsKey(party.get())) {
				if (!matchTeamParties.containsKey(match)) {
					matchTeamParties.put(match, new HashMap<>());
				}

				matchTeamParties.get(match).put(team, party.get());
				partyTeams.put(party.get(), team);
				party.get().setName(team.getName());
				return party.get();
			}
		}

		Party party = super.selectPartyForTeam(match, team);
		teamColours.put(team, party.getColor());
		return party;
	}

	/**
	 * Reset the colour preference for a given {@link Team}.
	 * 
	 * @param team The {@link Team} whose preference to reset
	 * @return Whether the team had a preference
	 */
	public boolean resetColour(Team team) {
		return teamColours.remove(team) != null;
	}

	/**
	 * Reset all {@link Team}s' colours preferences.
	 */
	public void resetColours() {
		teamColours.clear();
	}

}
