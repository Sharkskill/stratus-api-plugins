/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.requests;

import java.util.UUID;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;

/**
 * Represents a request to the API from a player wanting to create a custom kit
 * 
 * @author ShinyDialga
 *
 */
public class CustomKitCreateRequest implements Request<Void> {

	private UUID uuid;
	private String mapId;
	private String mapVersion;
	private int[] slots;

	public CustomKitCreateRequest(UUID uuid, String mapId, String mapVersion, int[] slots) {
		this.uuid = uuid;
		this.mapId = mapId;
		this.mapVersion = mapVersion;
		this.slots = slots;
	}

	public UUID getUuid() {
		return uuid;
	}

	public String getMapId() {
		return mapId;
	}

	public String getMapVersion() {
		return mapVersion;
	}

	public int[] getSlots() {
		return slots;
	}

	@Override
	public String getEndpoint() {
		return "/pgm/customkit/" + uuid.toString() + "/" + mapId;
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public Void make(APIClient client) {
		return client.put(this);
	}

}
