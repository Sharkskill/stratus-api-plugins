/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.teams;

import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Party;

/**
 * Matches up a {@link Party} to a {@link Team} in a given {@link Match}.
 * 
 * @author Ian Ballingall
 *
 */
public interface PartySelectStrategy {

	/**
	 * Select a {@link Party} for the given {@link Team} for this {@link Match}.
	 * 
	 * @param match The {@link Match} to assign in
	 * @param team  The {@link Team} to assign to
	 * @return The assigned {@link Party}
	 */
	Party select(Match match, Team team);

	/**
	 * Clean up decision-making data associated with a given {@link Match}.
	 * 
	 * @param match The {@link Match} whose data to clean up
	 */
	void cleanup(Match match);

}
