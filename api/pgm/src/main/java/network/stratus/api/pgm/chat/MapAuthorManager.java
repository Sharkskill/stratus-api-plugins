/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.chat;

import network.stratus.api.pgm.StratusAPIPGM;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.scheduler.BukkitRunnable;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.api.player.event.MatchPlayerAddEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Manages display names when using PGM, support match-dependent prefixes.
 * Presently allows for map author prefixes.
 *
 * @author Ian Ballingall
 */
public class MapAuthorManager extends PGMDisplayNameManager implements Listener {

	private static final String MAP_AUTHOR_PREFIX = ChatColor.BLUE + "*";
	private static final String PGM_PREMIUM = "pgm.premium";

	private final Map<UUID, List<PermissionAttachment>> attachments = new HashMap<>();

	@Override
	public String getPrefix(UUID uuid) {
		String prefixes = super.getPrefix(uuid);
		MatchPlayer player = PGM.get().getMatchManager().getPlayer(uuid);
		if (player != null && player.getMatch().getMap().getAuthors().stream().anyMatch(c -> c.isPlayer(uuid))) {
			prefixes = MAP_AUTHOR_PREFIX + prefixes;
			attach(player.getBukkit(), PGM_PREMIUM);
		}

		return prefixes;
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onMatchPlayerAdd(MatchPlayerAddEvent event) {
		detach(event.getPlayer().getBukkit());
		// TODO: is there a nicer way of doing this?
		new BukkitRunnable() {
			@Override
			public void run() {
				setDisplayName(event.getPlayer().getBukkit());
			}
		}.runTaskLater(StratusAPIPGM.get(), 1);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerQuit(PlayerQuitEvent event) {
		detach(event.getPlayer());
		attachments.remove(event.getPlayer().getUniqueId());
	}

	private void attach(Player player, String permission) {
		List<PermissionAttachment> permissions =
				attachments.computeIfAbsent(player.getUniqueId(), k -> new ArrayList<>());
		permissions.add(player.addAttachment(StratusAPIPGM.get(), permission, true));
	}

	private void detach(Player player) {
		List<PermissionAttachment> permissions = attachments.get(player.getUniqueId());
		if (permissions != null) {
			permissions.forEach(PermissionAttachment::remove);
			permissions.clear();
		}
	}

}
