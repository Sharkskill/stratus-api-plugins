package network.stratus.api.pgm.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.models.status.PGMState;
import network.stratus.api.bukkit.models.status.PGMStateMessage;
import network.stratus.api.bukkit.status.StatusUpdater;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.statistics.MatchStatistics;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Competitor;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.score.ScoreMatchModule;

public class PGMStatusUpdater {
    private static final PGMStatusUpdater instance = new PGMStatusUpdater();
    public static PGMStatusUpdater get() { return instance; }

    private int updateAttemptsSinceLastUpdate = 0;
    boolean dirty = true;

    private PGMStateMessage getPGMStateMessage() {
        Iterator<Match> matchIterator = PGM.get().getMatchManager().getMatches();
        int count = 0;
        Match match = null;
        while (matchIterator.hasNext()) {
            count++;
            match = matchIterator.next();
        }
        if (match == null) {
            StratusAPIPGM.get().getLogger().info("PGM Status updater: No match found");
            return new PGMStateMessage(StratusAPI.get().getServerName(), "nomatch", null);
        } else if (count > 1) {
            StratusAPIPGM.get().getLogger().info("PGM Status updater: Multiple matches found");
            return new PGMStateMessage(StratusAPI.get().getServerName(), "multiplematches", null);
        }

        String mapName = match.getMap().getName();
        String phase;
        switch (match.getPhase()) {
            case IDLE: phase = "idle"; break;
            case STARTING: phase = "starting"; break;
            case RUNNING: phase = "running"; break;
            case FINISHED: phase = "finished"; break;
            default: phase = "unknown"; break;
        }

        List<String> observers = new ArrayList<>();
        Map<String, String> currentTeam = new HashMap<>();
        Map<String, String> primaryTeam = new HashMap<>();

        for (Player player : Bukkit.getOnlinePlayers()) {
            MatchPlayer matchPlayer = match.getPlayer(player);
            if (!matchPlayer.isVisible()) { continue; }
            if (matchPlayer.isObserving()) {
                observers.add(player.getUniqueId().toString());
            } else if (matchPlayer.isParticipating()) {
                Party party = matchPlayer.getParty();
                if (party instanceof Competitor) {
                    currentTeam.put(player.getUniqueId().toString(), party.getNameLegacy());
                }
            }
        }

        MatchStatistics stats = StratusAPIPGM.get().getStatisticsManager().getMatchStatistics();
        String matchID = stats.getMatchId();

        Map<UUID, Map<String, AtomicInteger>> playtimes = stats.getPlaytime();
        for (UUID playerUUID : playtimes.keySet()) {
            Map<String, AtomicInteger> playerPlaytimes = playtimes.get(playerUUID);
            int max = 0;
            String maxTeam = null;
            for (String team : playerPlaytimes.keySet()) {
                int time = playerPlaytimes.get(team).get();
                if (time > max) {
                    max = time;
                    maxTeam = team;
                }
            }
            if (maxTeam != null) {
                primaryTeam.put(playerUUID.toString(), maxTeam);
            }
        }

        Map<String, Double> scores = new HashMap<>();
        Map<String, String> colors = new HashMap<>();
        ScoreMatchModule smm = match.moduleOptional(ScoreMatchModule.class).orElse(null);
        if (smm != null) {
            Map<Competitor, Double> pgmScores = smm.getScores();
            for (Competitor competitor : pgmScores.keySet()) {
                scores.put(competitor.getNameLegacy(), pgmScores.get(competitor));
                colors.put(competitor.getNameLegacy(), competitor.getColor().toString());
            }
        }

        long duration = match.getDuration().toMillis();

        return new PGMStateMessage(StratusAPI.get().getServerName(), null, new PGMState(
            matchID,
            mapName,
            phase,
            observers,
            currentTeam,
            primaryTeam,
            scores,
            colors,
            duration
        ));
    }

    private void updateStatus() {
        dirty = false;
        updateAttemptsSinceLastUpdate = 0;
        StatusUpdater.get().updatePGMState(getPGMStateMessage());
    }

    public void markDirty() {
        dirty = true;
    }

    public void updateIfNeeded() {
        updateAttemptsSinceLastUpdate++;
        if (dirty) {
            updateStatus();
        } else if (updateAttemptsSinceLastUpdate >= 10) {
            updateStatus();
        }
    }
}
