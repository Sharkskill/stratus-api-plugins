/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.commands;

import java.util.UUID;

import javax.annotation.Nullable;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.bukkit.parametric.annotation.Sender;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.models.UserData;
import network.stratus.api.bukkit.models.UserData.PGMStatistics;
import network.stratus.api.bukkit.requests.NameUserDataRequest;
import network.stratus.api.bukkit.requests.UserDataRequest;
import network.stratus.api.bukkit.requests.UuidUserDataRequest;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.statistics.MatchStatistics;

/**
 * Commands for viewing statistics.
 * 
 * @author Ian Ballingall
 *
 */
public class StatisticsCommands {

	@Command(aliases = { "statistics", "stats" },
			desc = "View your own or another player's statistics",
			usage = "[<player>]")
	public void viewStatistics(@Sender CommandSender sender, @Nullable TabCompletePlayer target) {
		UserDataRequest request;
		if (target == null) {
			request = new UuidUserDataRequest.Builder(((Player) sender).getUniqueId()).pgmStatistics().build();
		} else {
			request = new NameUserDataRequest.Builder(target.getUsername()).pgmStatistics().build();
		}

		StratusAPI.get().newSharedChain("statistics").<UserData>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			// Remote statistics
			UUID uuid = response.get_id();
			PGMStatistics stats = response.getPgmStatistics();

			// Current match statistics
			MatchStatistics matchStats = StratusAPIPGM.get().getStatisticsManager().getMatchStatistics();
			int kills = matchStats.getKills(uuid) + stats.getKills();
			int deaths = matchStats.getDeaths(uuid) + stats.getDeaths();
			int wools = matchStats.getWools(uuid) + stats.getWools();
			int monuments = matchStats.getMonuments(uuid) + stats.getMonuments();
			int cores = matchStats.getCores(uuid) + stats.getCores();
			int flags = matchStats.getFlags(uuid) + stats.getFlags();

			if (target == null) {
				audience.sendMessage("statistics.view.title.own");
			} else {
				audience.sendMessage("statistics.view.title.other", Chat.getDisplayName(response));
			}

			// If player has no deaths, KDR = kills to avoid divide by zero
			audience.sendMessage("statistics.view.killsdeaths", kills, deaths,
					(deaths == 0) ? (float) kills : (float) kills / deaths);

			audience.sendMessage("statistics.view.objectives.title");
			audience.sendMessage("statistics.view.objectives", wools, cores, monuments, flags);
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

}
