/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import org.bukkit.plugin.Plugin;
import tc.oc.pgm.util.bukkit.ViaUtils;

/**
 * Forces the maximum players to 60 for 1.7 clients (3 columns, 20 rows).
 *
 * @author Ian Ballingall
 */
public class LegacyTabMaxPlayersListener extends PacketAdapter {

	private static final Integer MAX_PLAYERS = 3 * 20; // 3 columns, 20 rows

	public LegacyTabMaxPlayersListener(Plugin plugin) {
		super(plugin, ListenerPriority.LOWEST, PacketType.Play.Server.LOGIN);
	}

	@Override
	public void onPacketSending(PacketEvent event) {
		if (event.getPacketType() != PacketType.Play.Server.LOGIN ||
				ViaUtils.getProtocolVersion(event.getPlayer()) > ViaUtils.VERSION_1_7)
			return;

		event.getPacket().getIntegers().write(2, MAX_PLAYERS);
	}
}
