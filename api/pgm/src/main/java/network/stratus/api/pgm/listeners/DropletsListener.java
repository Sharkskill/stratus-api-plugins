/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.listeners;


import com.google.common.collect.HashMultimap;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.unification.DropletsManager;
import network.stratus.api.pgm.models.droplets.DropletsMatchEndingRealm;
import network.stratus.api.pgm.statistics.MatchStatistics;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDespawnInVoidEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.match.event.MatchFinishEvent;
import tc.oc.pgm.api.match.event.MatchLoadEvent;
import tc.oc.pgm.api.party.Competitor;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.api.player.ParticipantState;
import tc.oc.pgm.api.player.event.MatchPlayerDeathEvent;
import tc.oc.pgm.goals.Goal;
import tc.oc.pgm.goals.GoalMatchModule;
import tc.oc.pgm.goals.ShowOption;
import tc.oc.pgm.score.PlayerScoreboxEvent;
import tc.oc.pgm.stats.PlayerStats;
import tc.oc.pgm.stats.StatsMatchModule;
import tc.oc.pgm.wool.MonumentWool;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;

/**
 * Listener methods pertaining to statistics. These listen to PGM events in
 * order to track player kills, deaths and objectives, and to know when matches
 * are starting and ending.
 * 
 * @author Ian Ballingall
 *
 */
public class DropletsListener implements Listener {

	private DropletsManager dropletsManager;
	private MatchStatistics	matchStatistics;

	private final Map<Item, UUID> droppedWools = new WeakHashMap<>();
	private final HashMultimap<DyeColor, UUID> destroyedWools = HashMultimap.create();

	public DropletsListener(DropletsManager dropletsManager) {
		this.dropletsManager = dropletsManager;
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void handleItemDrop(final PlayerDropItemEvent event) {
		ParticipantState player = PGM.get().getMatchManager().getParticipantState(event.getPlayer());
		if(player == null) return;

		Competitor team = player.getParty();
		Item itemDrop = event.getItemDrop();
		ItemStack item = itemDrop.getItemStack();

		if (this.isDestroyableWool(item, team)) {
			this.droppedWools.put(itemDrop, player.getId());
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void handleItemDespawn(final EntityDespawnInVoidEvent event) {
		Entity entity = event.getEntity();
		if (!(entity instanceof Item)) return;
		ItemStack stack = ((Item) entity).getItemStack();

		UUID playerId = this.droppedWools.remove(entity);
		if (playerId == null) return;

		ParticipantState player = PGM.get().getMatchManager().getParticipantState(playerId);
		if (player == null) return;

		if(isDestroyableWool(stack, player.getParty())) {
			giveWoolDestroyRaindrops(player, ((Wool) stack.getData()).getColor());
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void handleCraft(final CraftItemEvent event) {
		ParticipantState player = PGM.get().getMatchManager().getParticipantState(event.getWhoClicked());
		if (player == null) return;

		for (ItemStack ingredient : event.getInventory().getMatrix()) {
			if(this.isDestroyableWool(ingredient, player.getParty())) {
				giveWoolDestroyRaindrops(player, ((Wool) ingredient.getData()).getColor());
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onKillWoolCarrier(final MatchPlayerDeathEvent event) {
		ParticipantState killer = event.getKiller();
		if (killer == null) return;

//		killer.getPlayer().ifPresent(p -> {
//			String rotation = StratusAPIPGM.get().getDropletsRotation();
//			double count = dropletsManager.getDroplets().getMiscellaneous().getKillWoolCarrier().get(rotation);

			Set<Byte> killedWools = new HashSet<>();
			for (ItemStack itemStack : event.getParent().getDrops()) {
				if (isDestroyableWool(itemStack, killer.getParty()) &&
						!killedWools.contains(itemStack.getData().getData())) {
					killedWools.add(itemStack.getData().getData());
					StratusAPIPGM.get().getStatisticsManager().registerWoolCarrierKillDroplets(killer.getId());
//					this.dropletsManager.playEffect(count, p.getBukkit(), "unification.wool.killcarrier");
				}
			}
//		});
	}

	private void giveWoolDestroyRaindrops(final ParticipantState player, final DyeColor wool) {
		if(!this.destroyedWools.containsEntry(wool, player.getId())) {
			this.destroyedWools.put(wool, player.getId());
			if (!player.getPlayer().isPresent()) {
				return;
			}
//			String rotation = StratusAPIPGM.get().getDropletsRotation();
//			double count = this.dropletsManager.getDroplets().getMiscellaneous().getThrowAwayWool().get(rotation);
			StratusAPIPGM.get().getStatisticsManager().registerWoolDestroyDroplets(player.getId());
//			this.dropletsManager.playEffect(count, player.getPlayer().get().getBukkit(), "unification.wool.destroy");
		}
	}

	private boolean isDestroyableWool(ItemStack stack, Competitor team) {
		if(stack == null || stack.getType() != Material.WOOL) {
			return false;
		}

		DyeColor color = ((Wool) stack.getData()).getColor();
		boolean enemyOwned = false;

		for(Goal goal : team.getMatch().needModule(GoalMatchModule.class).getGoals()) {
			if(goal instanceof MonumentWool) {
				MonumentWool wool = (MonumentWool) goal;
				if(wool.hasShowOption(ShowOption.STATS) && !wool.isPlaced() && wool.getDyeColor() == color) {
					if(wool.getOwner() == team) {
						return false;
					} else {
						enemyOwned = true;
					}
				}
			}
		}

		return enemyOwned;
	}

//	@EventHandler(priority = EventPriority.MONITOR)
//	public void onCoreLeak(CoreLeakEvent event) {
//		Set<ParticipantState> touchingPlayers = event.getCore().getTouchingPlayers();
//		String rotation = StratusAPIPGM.get().getDropletsRotation();
//		double count = dropletsManager.getDroplets().getObjectives().getCoreLeak().get(rotation) *
//				(2.0 / (touchingPlayers.size() + 1.0));
//		for (ParticipantState ps : touchingPlayers) {
//			ps.getPlayer().ifPresent(p ->
//					dropletsManager.playEffect(count, p.getBukkit(), "unification.core.leak"));
//		}
//	}

//	@EventHandler(priority = EventPriority.MONITOR)
//	public void onMonumentBreak(DestroyableDestroyedEvent event) {
//		String rotation = StratusAPIPGM.get().getDropletsRotation();
//		double count = dropletsManager.getDroplets().getObjectives().getMonumentBreak().get(rotation);
//		for (DestroyableContribution dc : event.getDestroyable().getContributions()) {
//			dc.getPlayerState().getPlayer().ifPresent(p ->
//					dropletsManager.playEffect(count, p.getBukkit(), "unification.monument.break"));
//		}
//	}

//	@EventHandler(priority = EventPriority.MONITOR)
//	public void onWoolCapture(PlayerWoolPlaceEvent event) {
//		String rotation = StratusAPIPGM.get().getDropletsRotation();
//		double count = dropletsManager.getDroplets().getObjectives().getWoolCap().get(rotation);
//		event.getPlayer().getPlayer().ifPresent(p ->
//				dropletsManager.playEffect(count, p.getBukkit(), "unification.wool.capture"));
//	}
//
//	@EventHandler(priority = EventPriority.MONITOR)
//	public void onFlagCapture(FlagCaptureEvent event) {
//		String rotation = StratusAPIPGM.get().getDropletsRotation();
//		double count = dropletsManager.getDroplets().getObjectives().getFlagCap().get(rotation);
//		dropletsManager.playEffect(count, event.getCarrier().getBukkit(), "unification.flag.capture");
//	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onScorebox(PlayerScoreboxEvent event) {
		StratusAPIPGM.get().getStatisticsManager().registerScorebox(event.getPlayer(), event.getPoints());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onMatchFinish(MatchFinishEvent event) {
		Match match = event.getMatch();
		StatsMatchModule smm = match.needModule(StatsMatchModule.class);
		String rotation = StratusAPIPGM.get().getDropletsRotation();

		if (smm == null) {
		    return;
        }

		Map<UUID, Integer> allKills = new HashMap<>();

		for (Map.Entry<UUID, PlayerStats> mapEntry : smm.getAllPlayerStats().entrySet()) {
			UUID playerUUID = mapEntry.getKey();
			PlayerStats playerStats = mapEntry.getValue();

			smm.getPlayerStat(playerUUID);

			allKills.put(playerUUID, playerStats.getKills());
		}

		Map.Entry<UUID, Integer> topKiller =
				allKills.entrySet().stream().max(Comparator.comparingInt(Map.Entry::getValue)).orElse(null);
		if (topKiller != null && topKiller.getValue() > 0) {
            double count = dropletsManager.getDroplets().getMiscellaneous().getTopKiller().get(rotation);

            dropletsManager.playEffect(count, topKiller.getKey(), "droplets.topkiller");
        }

        Map<String, DropletsMatchEndingRealm> endingRealmMap = rotation.equals("ranked") ?
				dropletsManager.getDroplets().getMatchEnding().getRanked() :
				dropletsManager.getDroplets().getMatchEnding().getMixed();

		DropletsMatchEndingRealm winDroplets = endingRealmMap.get("win");
		DropletsMatchEndingRealm lossDroplets = endingRealmMap.get("loss");

		if (match.getParticipants().size() > 1) {
			for (MatchPlayer player : match.getParticipants()) {
				boolean winner = match.getWinners().contains(player.getCompetitor());
				DropletsMatchEndingRealm realm = winner ? winDroplets : lossDroplets;
				double count = realm.getInitial() +
						((matchStatistics.getPlaytime(player.getId()) / 60) * realm.getMinute());

				dropletsManager.playEffect(count, player.getBukkit(), "droplets.match.end." + (winner ? "win" : "loss"));
			}
		}

		this.droppedWools.clear();
		this.destroyedWools.clear();
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onMatchLoad(MatchLoadEvent event) {
		if (StratusAPIPGM.get().getStatisticsManager() != null) {
			//Wait so that match statistics can load
			StratusAPIPGM.get().getServer().getScheduler().runTaskLater(StratusAPIPGM.get(), () -> {
				matchStatistics = StratusAPIPGM.get().getStatisticsManager().getMatchStatistics();
			}, 5);
		}
	}

}
