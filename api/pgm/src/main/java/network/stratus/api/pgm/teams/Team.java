/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.teams;

import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents a team which can be registered for participation in matches. This
 * corresponds to a roster, and is intended for use in events and tournaments.
 * This does not directly correspond to a team in the backend, but may
 * correspond to a roster for a team's registration.
 * 
 * @see {@link network.stratus.api.models.Team} for the representation of a
 *      team in the backend.
 * 
 * @author Ian Ballingall
 *
 */
public class Team {

	private String _id;
	private String name;
	private Set<Player> players;

	public Team() {
	}

	public Team(String _id, String name, Set<Player> players) {
		this._id = _id;
		this.name = name;
		this.players = players;
	}

	public String get_id() {
		return _id;
	}

	public String getName() {
		return name;
	}

	public Set<Player> getPlayers() {
		return players;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Team other = (Team) obj;
		if (_id == null) {
			if (name == null) {
				return false;
			} else {
				if (other.name == null) {
					return false;
				} else {
					return name.equals(other.name);
				}
			}
		} else {
			if (other._id == null) {
				return false;
			} else {
				return _id.equals(other._id);
			}
		}
	}

	/**
	 * Represents a player who forms part of a {@link Team}. These players may or
	 * may not be allowed to veto, if the format supports it.
	 */
	public static class Player {

		@JsonProperty("_id")
		private UUID uuid;
		private boolean canVeto;

		public Player() {
		}

		public Player(UUID uuid, boolean canVeto) {
			this.uuid = uuid;
			this.canVeto = canVeto;
		}

		@JsonProperty("_id")
		public UUID getUuid() {
			return uuid;
		}

		public boolean canVeto() {
			return canVeto;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Player other = (Player) obj;
			if (uuid == null) {
				if (other.uuid != null)
					return false;
			} else if (!uuid.equals(other.uuid))
				return false;
			return true;
		}

	}

}
