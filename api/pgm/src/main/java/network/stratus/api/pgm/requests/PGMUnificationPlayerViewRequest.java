/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.requests;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.pgm.models.UnificationPlayer;
import network.stratus.api.pgm.unification.Droplets;

import java.util.List;
import java.util.UUID;

/**
 * Represents a request to the API for fetching unification data
 * 
 * @author ShinyDialga
 * @author Ian Ballingall
 *
 */
public class PGMUnificationPlayerViewRequest implements Request<UnificationPlayer> {

	private UUID uuid;
	private List<String> realms;

	public PGMUnificationPlayerViewRequest(UUID uuid, List<String> realms) {
		this.uuid = uuid;
		this.realms = realms;
	}

	@Override
	public String getEndpoint() {
		return "/pgm/unification/" + uuid + "/?realms=" + String.join(",", realms);
	}

	public UUID getUuid() {
		return uuid;
	}

	public List<String> getRealms() {
		return realms;
	}

	@Override
	public Class<UnificationPlayer> getResponseType() {
		return UnificationPlayer.class;
	}

	@Override
	public UnificationPlayer make(APIClient client) {
		return client.get(this);
	}

}
