/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.statistics;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.pgm.unification.MatchDroplets;

/**
 * Tracks player statistics over the course of a
 * {@link tc.oc.pgm.api.match.Match}.
 *
 * @author Ian Ballingall
 *
 */
public class MatchStatistics implements Request<Void> {

	private String matchId;
	private String server;
	private AtomicInteger duration;
	private String map;
	private String mapSlug;
	private Set<MatchStatisticTeam> teams;
	private Map<UUID, String> playerTeams;
	private Set<UUID> trackedPlayers;
	private Set<UUID> winners;
	private Set<UUID> losers;
	private Map<UUID, AtomicInteger> kills;
	private Map<UUID, AtomicInteger> deaths;
	private Map<UUID, AtomicInteger> wools;
	private Map<UUID, AtomicInteger> monuments;
	private Map<UUID, AtomicInteger> cores;
	private Map<UUID, AtomicInteger> flags;
	private Map<UUID, AtomicInteger> scoreboxes;
	private Map<UUID, AtomicInteger> goldenApplesEaten;
	private Map<UUID, Map<String, AtomicInteger>> playtime;
	private Map<UUID, Set<String>> touches;
	private Map<UUID, Set<String>> firstTouches;
	private Map<UUID, Map<String, AtomicInteger>> questProgress;
	private Vector<String> events;

	private MatchRanked ranked;

	private MatchDroplets droplets;

	private Map<UUID, Map<String, Object>> matchStats;

	/** Indicates whether the corresponding match is running. */
	@JsonIgnore
	private boolean matchActive;

	public MatchStatistics() {
		this.matchId = new StringBuilder().append(System.currentTimeMillis()).reverse().toString();
		this.server = StratusAPI.get().getServerName();
		this.duration = new AtomicInteger();
		this.trackedPlayers = new HashSet<>();
		this.playerTeams = new HashMap<>();
		this.teams = new HashSet<>();
		this.winners = new HashSet<>();
		this.losers = new HashSet<>();
		this.kills = new HashMap<>();
		this.deaths = new HashMap<>();
		this.wools = new HashMap<>();
		this.monuments = new HashMap<>();
		this.cores = new HashMap<>();
		this.flags = new HashMap<>();
		this.scoreboxes = new HashMap<>();
		this.goldenApplesEaten = new HashMap<>();
		this.touches = new HashMap<>();
		this.firstTouches = new HashMap<>();
		this.questProgress = new HashMap<>();
		this.playtime = new ConcurrentHashMap<>();
		this.events = new Vector();
		this.matchActive = true;

		this.ranked = new MatchRanked();

		this.droplets = new MatchDroplets();

		this.matchStats = new HashMap<>();
	}

	public String getServer() {
		return server;
	}

	public Set<UUID> getTrackedPlayers() {
		return trackedPlayers;
	}

	public Set<MatchStatisticTeam> getTeams() {
		return teams;
	}

	public Set<UUID> getWinners() {
		return winners;
	}

	public Set<UUID> getLosers() {
		return losers;
	}

	public Map<UUID, AtomicInteger> getKills() {
		return kills;
	}

	public Map<UUID, AtomicInteger> getDeaths() {
		return deaths;
	}

	public Map<UUID, AtomicInteger> getWools() {
		return wools;
	}

	public Map<UUID, AtomicInteger> getMonuments() {
		return monuments;
	}

	public Map<UUID, AtomicInteger> getCores() {
		return cores;
	}

	public Map<UUID, AtomicInteger> getFlags() {
		return flags;
	}

	public Map<UUID, AtomicInteger> getScoreboxes() {
		return scoreboxes;
	}

	public Map<UUID, AtomicInteger> getGoldenApplesEaten() {
		return goldenApplesEaten;
	}

	public Map<UUID, Map<String, AtomicInteger>> getPlaytime() {
		return playtime;
	}

	public Map<UUID, Set<String>> getTouches() {
		return touches;
	}

	public Map<UUID, Set<String>> getFirstTouches() {
		return firstTouches;
	}

	public MatchRanked getRanked() {
		return ranked;
	}

	public MatchDroplets getDroplets() {
		return droplets;
	}

	public Map<UUID, Map<String, AtomicInteger>> getQuestProgress() {
        return questProgress;
    }

	public Vector<String> getEvents() {
		return events;
	}

	public String getMatchId() {
		return matchId;
	}

	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}

	public Map<UUID, String> getPlayerTeams() {
		return playerTeams;
	}

	public AtomicInteger getDuration() {
		return duration;
	}

	public String getMap() {
		return map;
	}

	public void setMap(String map) {
		this.map = map;
	}

	public String getMapSlug() {
		return mapSlug;
	}

	public void setMapSlug(String mapSlug) {
		this.mapSlug = mapSlug;
	}

	public Map<UUID, Map<String, Object>> getMatchStats() {
		return matchStats;
	}

	public void setMatchStats(Map<UUID, Map<String, Object>> matchStats) {
		this.matchStats = matchStats;
	}

	@JsonIgnore
	protected boolean isMatchActive() {
		return matchActive;
	}

	@JsonIgnore
	protected void setMatchActive(boolean matchActive) {
		this.matchActive = matchActive;
	}

	/**
	 * Get a player's kills in the current match by their UUID. This will return
	 * their current kill count if the match is ongoing, and 0 otherwise.
	 *
	 * @param uuid The UUID of the player whose kills to find
	 * @return The number of kills the player has in this match
	 */
	@JsonIgnore
	public int getKills(UUID uuid) {
		return matchActive ? kills.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

	/**
	 * Get a player's deaths in the current match by their UUID. This will return
	 * their current death count if the match is ongoing, and 0 otherwise.
	 *
	 * @param uuid The UUID of the player whose deaths to find
	 * @return The number of deaths the player has in this match
	 */
	@JsonIgnore
	public int getDeaths(UUID uuid) {
		return matchActive ? deaths.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

	/**
	 * Get a player's wools in the current match by their UUID. This will return
	 * their current wool count if the match is ongoing, and 0 otherwise.
	 *
	 * @param uuid The UUID of the player whose wools to find
	 * @return The number of wools the player has in this match
	 */
	@JsonIgnore
	public int getWools(UUID uuid) {
		return matchActive ? wools.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

	/**
	 * Get a player's monuments in the current match by their UUID. This will return
	 * their current count if the match is ongoing, and 0 otherwise.
	 *
	 * @param uuid The UUID of the player whose monuments to find
	 * @return The number of monuments the player has in this match
	 */
	@JsonIgnore
	public int getMonuments(UUID uuid) {
		return matchActive ? monuments.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

	/**
	 * Get a player's cores in the current match by their UUID. This will return
	 * their current core count if the match is ongoing, and 0 otherwise.
	 *
	 * @param uuid The UUID of the player whose cores to find
	 * @return The number of cores the player has in this match
	 */
	@JsonIgnore
	public int getCores(UUID uuid) {
		return matchActive ? cores.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

	/**
	 * Get a player's flags in the current match by their UUID. This will return
	 * their current flag count if the match is ongoing, and 0 otherwise.
	 *
	 * @param uuid The UUID of the player whose flags to find
	 * @return The number of flags the player has in this match
	 */
	@JsonIgnore
	public int getFlags(UUID uuid) {
		return matchActive ? flags.getOrDefault(uuid, new AtomicInteger()).intValue() : 0;
	}

	/**
	 * Get a player's playtime in the current match in minutes. by their UUID. This
	 * will return their current playtime if the match is running, and 0 otherwise.
	 *
	 * @param uuid The UUID of the player whose playtime to find
	 * @return The player's playtime
	 */
	@JsonIgnore
	public int getPlaytime(UUID uuid) {
		return matchActive ? playtime.getOrDefault(uuid, new HashMap<>()).values().stream()
				.mapToInt(AtomicInteger::get).sum() : 0;
	}

	@Override
	public String getEndpoint() {
		return "/pgm/statistics";
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public Void make(APIClient client) {
		return client.post(this);
	}

}
