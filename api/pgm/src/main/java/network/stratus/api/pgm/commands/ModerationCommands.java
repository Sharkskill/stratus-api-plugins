/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.commands;

import app.ashcon.intake.Command;
import app.ashcon.intake.bukkit.parametric.annotation.Sender;
import network.stratus.api.pgm.moderation.FreezeManager;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Moderation commands that depend on PGM.
 *
 * @author Meeples10
 * @see network.stratus.api.bukkit.commands.PunishmentCommands
 */
public class ModerationCommands {

	private FreezeManager freezeManager;

	public ModerationCommands(FreezeManager freezeManager) {
		this.freezeManager = freezeManager;
	}

	@Command(aliases = { "freeze", "f" },
			desc = "Freeze a player",
			perms = "stratusapi.command.freeze",
			usage = "<player>")
	public void onFreeze(@Sender CommandSender sender, Player target) {
		freezeManager.toggleFreeze((Player) sender, target);
	}
}