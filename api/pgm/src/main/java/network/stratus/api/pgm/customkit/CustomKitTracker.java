/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.customkit;

import network.stratus.api.pgm.models.CustomKit;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Nullable;

/**
 * Tracks players' custom kits per map.
 * 
 * @author ShinyDialga
 * @author Ian Ballingall
 *
 */
public class CustomKitTracker {

	private Map<UUID, Map<String, CustomKit>> tracker;

	public CustomKitTracker() {
		this.tracker = new HashMap<>();
	}

	/**
	 * Remove all kits corresponding to a player.
	 * 
	 * @param uuid The player's {@link UUID}
	 */
	public void removePlayer(UUID uuid) {
		tracker.remove(uuid);
	}

	/**
	 * Store a kit for a player.
	 * 
	 * @param uuid  The player's {@link UUID}
	 * @param mapId The map's identifier string
	 * @param kit   The {@link CustomKit} to store for the map
	 */
	public void addKit(UUID uuid, String mapId, CustomKit kit) {
		Map<String, CustomKit> kits = tracker.get(uuid);
		if (kits == null) {
			kits = new HashMap<>();
			tracker.put(uuid, kits);
		}

		kits.put(mapId, kit);
	}

	/**
	 * Remove a kit for a given player on a given map.
	 * 
	 * @param uuid  The player's {@link UUID}
	 * @param mapId The map's identifier string
	 * @return Whether a corresponding kit was removed
	 */
	public boolean removeKit(UUID uuid, String mapId) {
		Map<String, CustomKit> kits = tracker.get(uuid);
		if (kits != null) {
			return kits.remove(mapId) != null;
		}

		return false;
	}

	/**
	 * Get all stored kits for a player. This will only contain kits that are loaded
	 * on the server; some kits might be stored on the backend but not loaded.<br>
	 * This can be {@code null} if no kits have been stored for this player.
	 * 
	 * @param uuid The player's {@link UUID}
	 * @return The {@link Map} of map identifier strings to {@link CustomKit}s
	 */
	@Nullable
	public Map<String, CustomKit> getKits(UUID uuid) {
		return tracker.get(uuid);
	}

	/**
	 * Get the kit for a player on a given map.<br>
	 * If no such kit is stored on the server, this will return {@code null}.
	 * 
	 * @param uuid  The player's {@link UUID}
	 * @param mapId The map's identifier string
	 * @return The corresponding {@link CustomKit}, if it exists
	 */
	@Nullable
	public CustomKit getCustomKit(UUID uuid, String mapId) {
		Map<String, CustomKit> kits = tracker.get(uuid);
		if (kits != null) {
			return kits.get(mapId);
		}

		return null;
	}

	/**
	 * Get the kit for a player on a given map, if the map version matches.<br>
	 * If no kit exists or the version doesn't match, this will return {@code null}.
	 * 
	 * @param uuid       The player's {@link UUID}
	 * @param mapId      The map's identifier string
	 * @param mapVersion The map version as a string
	 * @return The corresponding {@link CustomKit}, if it exists
	 */
	@Nullable
	public CustomKit getCustomKit(UUID uuid, String mapId, String mapVersion) {
		CustomKit kit = getCustomKit(uuid, mapId);
		if (kit != null && kit.getMapVersion().equals(mapVersion)) {
			return kit;
		} else {
			return null;
		}
	}
}
