package network.stratus.api.pgm.unification;

public enum QuestCondition {
    KILL("kill"),
    KILL_BOW("kill-bow"),
    KILL_SWORD("kill-sword"),
    TOUCH_OBJECTIVE("touch-objective"),
    COMPLETE_OBJECTIVE("complete-objective"),
    EAT_GOLDEN_APPLE("eat-golden-apple"),
    BLOCK_SHOT_25("25-block-shot");

    private String condition;

    QuestCondition(String condition) {
        this.condition = condition;
    }

    public String getCondition() {
        return condition;
    }

    @Override
    public String toString() {
        return condition;
    }
}
