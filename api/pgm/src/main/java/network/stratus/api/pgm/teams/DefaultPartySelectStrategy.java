/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.teams;

import java.util.HashMap;
import java.util.Map;

import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.teams.TeamMatchModule;

/**
 * Default strategy, iteratively allocating competing {@link Party}s to
 * {@link Team}s. The allocations are retained until {@link this#cleanup(Match)} is
 * called, and are retrieved when attempting to select, if available.
 * 
 * @author Ian Ballingall
 *
 */
public class DefaultPartySelectStrategy implements PartySelectStrategy {

	/** Maps teams to their parties, per match. */
	protected Map<Match, Map<Team, Party>> matchTeamParties;
	/** Maps parties to their teams. */
	protected Map<Party, Team> partyTeams;

	public DefaultPartySelectStrategy() {
		matchTeamParties = new HashMap<>();
		partyTeams = new HashMap<>();
	}

	@Override
	public Party select(Match match, Team team) {
		if (matchTeamParties.containsKey(match) && matchTeamParties.get(match).containsKey(team)) {
			return matchTeamParties.get(match).get(team);
		}

		return selectPartyForTeam(match, team);
	}

	protected Party selectPartyForTeam(Match match, Team team) {
		for (tc.oc.pgm.teams.Team party : match.needModule(TeamMatchModule.class).getTeams()) {
			if (!partyTeams.containsKey(party)) {
				if (!matchTeamParties.containsKey(match)) {
					matchTeamParties.put(match, new HashMap<>());
				}

				matchTeamParties.get(match).put(team, party);
				partyTeams.put(party, team);
				party.setName(team.getName());
				return party;
			}
		}

		throw new IllegalStateException(
				String.format("Could not assign team to %s in match %s", team.getName(), match.getId()));
	}

	@Override
	public void cleanup(Match match) {
		matchTeamParties.remove(match);
		match.getParties().forEach(partyTeams::remove);
	}

}
