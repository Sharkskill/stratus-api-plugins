package network.stratus.api.pgm.unification;

import java.util.HashMap;
import java.util.Map;

public class TokenManager {
    private Map<String, SetNextRotations> tokens;

    public TokenManager() {
        this.tokens = new HashMap<>();
    }

    public Map<String, SetNextRotations> getTokens() {
        return tokens;
    }
}
