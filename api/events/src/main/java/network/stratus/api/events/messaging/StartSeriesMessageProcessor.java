package network.stratus.api.events.messaging;

import java.io.IOException;

import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.CommandException;

import com.google.common.collect.Iterators;

import dev.pgm.events.EventsPlugin;
import dev.pgm.events.TournamentManager;
import dev.pgm.events.format.TournamentState;
import dev.pgm.events.xml.MapFormatXMLParser;
import network.stratus.api.events.StratusAPIEvents;
import network.stratus.api.messaging.MessageProcessor;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;

public class StartSeriesMessageProcessor implements MessageProcessor<StartSeries> {

    public StartSeriesMessageProcessor() {}

    /**
     * Logs a message about starting the specified match.
     */
    private void log(StartSeries object, String message) {
        System.out.println("[TM " + object.getSeriesID() + "] " + message);
    }

    /**
     * Logs the reason the match can't start, and sends discord an error, if one is needed.
     */
    private void reject(StartSeries object, String consoleError, @Nullable String discordError) {
        log(object, "Rejected - " + consoleError);
        if (discordError != null) {
            try {
                StratusAPIEvents.get().getStartedSeriesPublisher().publish(new StartedSeries(object.getSeriesID(), discordError));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void process(StartSeries object) {
        log(object, "Being queried about starting a series");

        if (!object.getServer().toLowerCase().equals(StratusAPIEvents.get().getServerName().toLowerCase())) {
            reject(
                object,
                "server name \""
                    + StratusAPIEvents.get().getServerName()
                    + "\" does not match requested server name \""
                    + object.getServer()
                    + "\"",
                // No need to send a message about the error - the backend likely is talking to a different server
                null
            );
            return;
        }

        if (!StratusAPIEvents.get().getRemote()) {
            reject(object, "remote starts not supported here", "That server doesn't support starting automatically");
            return;
        }

        if (StratusAPIEvents.get().hasSeriesReady()) {
            reject(object, "series already about to start", "There's already a series queued here!");
            return;
        }

        TournamentManager tm = EventsPlugin.get().getTournamentManager();
        if (!tm.currentTournament()
            .map((tournament) -> tournament.state().equals(TournamentState.FINISHED))
            .orElse(true)
            // If there's no one on the server, we don't care about half-finished tournament series.
            && Bukkit.getOnlinePlayers().size() > 0
        ) {
            reject(object, "match already in progress", "Server is already in use!");
            return;
        }

        Match currentMatch = Iterators.getNext(PGM.get().getMatchManager().getMatches(), null);
        if (currentMatch != null && currentMatch.isRunning()) {
            reject(object, "some match is already running", "Someone is already playing here");
        }

        if (EventsPlugin.get().getTeamRegistry().getTeam(object.getTeamA()) == null) {
            reject(object, "couldn't find team " + object.getTeamA(), "No team called " + object.getTeamA());
            return;
        }

        if (EventsPlugin.get().getTeamRegistry().getTeam(object.getTeamB()) == null) {
            reject(object, "couldn't find team " + object.getTeamB(), "No team called " + object.getTeamB());
            return;
        }

        if (object.getTeamA().toLowerCase().equals(object.getTeamB().toLowerCase())) {
            reject(object, "team cannot play themselves " + object.getTeamA(), object.getTeamA() + " can't play themselves");
            return;
        }

        if (MapFormatXMLParser.parse(object.getFormat()) == null) {
            reject(object, "couldn't find format " + object.getFormat(), "No format " + object.getFormat());
            return;
        }

        log(object, "Accepting!");

        Bukkit.getScheduler().runTask(StratusAPIEvents.get(), () -> {
            StratusAPIEvents.get().queueSeries(object);

            try {
                StratusAPIEvents.get().getStartedSeriesPublisher().publish(new StartedSeries(object.getSeriesID(), ""));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
