/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.events;

import app.ashcon.intake.bukkit.BukkitIntake;
import app.ashcon.intake.bukkit.graph.BasicBukkitCommandGraph;
import app.ashcon.intake.fluent.DispatcherNode;
import dev.pgm.events.EventsPlugin;
import dev.pgm.events.team.TournamentPlayer;
import dev.pgm.events.team.TournamentTeam;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.messaging.ConsumerExceptionHandler;
import network.stratus.api.events.commands.EventsCommands;

import network.stratus.api.events.listeners.EventsListener;
import network.stratus.api.events.messaging.FindSeriesServer;
import network.stratus.api.events.messaging.FindSeriesServerMessageProcessor;
import network.stratus.api.events.messaging.StartSeries;
import network.stratus.api.events.messaging.StartSeriesMessageProcessor;
import network.stratus.api.events.messaging.StartedSeries;
import network.stratus.api.events.requests.GetTeamsRequest;
import network.stratus.api.events.responses.GetTeamsResponse;
import network.stratus.api.messaging.JsonConsumer;
import network.stratus.api.messaging.JsonPublisher;
import network.stratus.api.messaging.RabbitMessenger;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.CommandException;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Iterators;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import javax.annotation.Nullable;

public class StratusAPIEvents extends JavaPlugin {

	private static StratusAPIEvents plugin;

	private String webhook;
	private String serverName;
	private String group;
	private boolean remote;

	private RabbitMessenger messenger;
	private JsonPublisher foundSeriesServerPublisher;
	private JsonPublisher startedSeriesPublisher;

	private @Nullable StartSeries startSeries = null;
	private long queueTime = 0;

	@Override
	public void onEnable() {
		plugin = this;

		saveDefaultConfig();

		// Set up commands
		BasicBukkitCommandGraph cmdGraph = new BasicBukkitCommandGraph();
		DispatcherNode root = cmdGraph.getRootDispatcherNode().registerNode("stratusevents");
		root.registerCommands(new EventsCommands());
		new BukkitIntake(this, cmdGraph).register();

		if (getConfig().getBoolean("auto-refresh", true)) {
			this.refreshTeams();
		}

		webhook = getConfig().getString("webhook");
		serverName = getConfig().getString("serverName", "unknown");
		group = getConfig().getString("group", "official");
		remote = getConfig().getBoolean("remote", false);

		getServer().getPluginManager().registerEvents(new EventsListener(), this);

		if (getConfig().getBoolean("messaging.enabled", true)) {
			messenger = new RabbitMessenger(getConfig().getString("messaging.uri"), new ConsumerExceptionHandler());
			try {
				messenger.initialiseConnection();
			} catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException | IOException
					| TimeoutException | UnrecoverableKeyException | KeyStoreException | CertificateException e) {
				getLogger().severe("Failed to connect to RabbitMQ server: " + e);
				getLogger().severe("Messaging services will be disabled");
				e.printStackTrace();
				messenger = null;
			}

			try {
				new JsonConsumer.Builder<FindSeriesServer>(messenger.getNewChannel(), FindSeriesServer.class)
						.consumerName("findseriesserver").exchangeName("tm.findseriesserver").exchangeType(BuiltinExchangeType.TOPIC)
						.routingKey("json").messageProcessor(new FindSeriesServerMessageProcessor()).build()
						.register();
			} catch (IOException e) {
				getLogger().severe("Failed to enable processor: " + e);
			}

			try {
				new JsonConsumer.Builder<StartSeries>(messenger.getNewChannel(), StartSeries.class)
						.consumerName("startseries").exchangeName("tm.startseries").exchangeType(BuiltinExchangeType.TOPIC)
						.routingKey("json").messageProcessor(new StartSeriesMessageProcessor()).build()
						.register();
			} catch (IOException e) {
				getLogger().severe("Failed to enable processor: " + e);
			}

			try {
				Channel publisherChannel = messenger.getNewChannel();
				publisherChannel.exchangeDeclare("tm.foundseriesserver", BuiltinExchangeType.TOPIC);
				foundSeriesServerPublisher = new JsonPublisher(publisherChannel, "tm.foundseriesserver", "json",
						new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("UTF-8")
								.deliveryMode(1).build());
			} catch (IOException e) {
				getLogger().severe("Failed to enable publisher: " + e);
			}

			try {
				Channel publisherChannel = messenger.getNewChannel();
				publisherChannel.exchangeDeclare("tm.startedseries", BuiltinExchangeType.TOPIC);
				startedSeriesPublisher = new JsonPublisher(publisherChannel, "tm.startedseries", "json",
						new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("UTF-8")
								.deliveryMode(1).build());
			} catch (IOException e) {
				getLogger().severe("Failed to enable publisher: " + e);
			}
		}

		getLogger().info("Stratus API Events extensions enabled");
	}

	@Override
	public void onDisable() {
		plugin = null;
		getLogger().info("Stratus API Events extensions disabled");
	}

	public static StratusAPIEvents get() {
		if (plugin == null)
			throw new IllegalStateException("Plugin is not enabled");

		return plugin;
	}

	public String getWebhook() {
		return webhook;
	}

	public JsonPublisher getFoundSeriesServerPublisher() {
		return foundSeriesServerPublisher;
	}

	public JsonPublisher getStartedSeriesPublisher() {
		return startedSeriesPublisher;
	}

	public void postWebhook(String content) {
		StratusAPI.get().newSharedChain("webhook").<Void>asyncFirst(() -> {
			try {
				DiscordWebhook discord = new DiscordWebhook(webhook);
				discord.setUsername("Albert");
				discord.setContent(content);
				discord.execute();
			} catch (IOException e) {

			}
			return null;
		}).execute();
	}

	public void refreshTeams() {
		GetTeamsRequest request = new GetTeamsRequest();
		StratusAPI.get().newSharedChain("teams").<GetTeamsResponse>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			EventsPlugin.get().getTeamRegistry().getTeams().clear();
			response.getTeams().forEach(team -> {
				List<TournamentPlayer> players = new ArrayList<>();
				players.add(TournamentPlayer.create(UUID.fromString(team.getLeader()), true));
				team.getPlayers().forEach(player -> players.add(TournamentPlayer.create(UUID.fromString(player.getUuid()), true)));
				TournamentTeam tournamentTeam = TournamentTeam.create(team.getName(), players);
				EventsPlugin.get().getTeamRegistry().addTeam(tournamentTeam);
			});
			getLogger().info("Stratus API Events - Teams refreshed. " +
					EventsPlugin.get().getTeamRegistry().getTeams().size() + " teams loaded.");
		}).execute();
	}

	public String getServerName() {
		return serverName;
	}

	public String getGroup() {
		return group;
	}

	public boolean getRemote() {
		return remote;
	}

	public boolean hasSeriesReady() {
		return startSeries != null;
	}

	public void queueSeries(StartSeries startSeries) {
		this.startSeries = startSeries;
		this.queueTime = System.currentTimeMillis();
		startSeriesIfPossible(false);
	}

	public void startSeriesIfPossible(boolean loadingMatch) {
		System.out.println("[TM] Trying to start a series");
		if (this.startSeries == null) {
			System.out.println("[TM] Nothing queued");
			return;
		}

		if (!loadingMatch) {
			Match currentMatch = Iterators.getNext(PGM.get().getMatchManager().getMatches(), null);
			if (currentMatch == null) {
				System.out.println("[TM " + this.startSeries.getSeriesID() + "] Server not yet ready");
				return;
			}
		}

		StartSeries series = this.startSeries;
		System.out.println("[TM " + this.startSeries.getSeriesID() + "] Starting!");
		this.startSeries = null;
		Bukkit.getScheduler().runTask(StratusAPIEvents.get(), () -> {
			Server server = getServer();
			try {
				server.dispatchCommand(server.getConsoleSender(), "tm unregisterall");
				server.dispatchCommand(server.getConsoleSender(), "tm register " + series.getTeamA());
				server.dispatchCommand(server.getConsoleSender(), "tm register " + series.getTeamB());
				server.dispatchCommand(server.getConsoleSender(), "tm create " + series.getFormat());
				Bukkit.getScheduler().runTaskLater(StratusAPIEvents.get(), () -> {
					int elapsedTime = (int) ((System.currentTimeMillis() - this.queueTime) / 1000);
					int newTime = Math.max(series.getStartTime() - elapsedTime, 30);

					server.dispatchCommand(server.getConsoleSender(), "start " + newTime);
				}, 100L);
			} catch (CommandException e) {
				e.printStackTrace();
			}
	});
	}
}
