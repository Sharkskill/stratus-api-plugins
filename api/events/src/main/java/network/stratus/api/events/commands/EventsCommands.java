/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.events.commands;

import app.ashcon.intake.Command;
import dev.pgm.events.EventsPlugin;
import network.stratus.api.events.StratusAPIEvents;
import network.stratus.api.pgm.StratusAPIPGM;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Refreshes tournament teams
 *
 * @author ShinyDialga
 */
public class EventsCommands {

	@Command(aliases = "refresh",
			desc = "Refresh the tournament teams",
			perms = "stratusapi.tournament.referee.refresh")
	public void refresh(CommandSender sender) {
		StratusAPIEvents.get().refreshTeams();
		sender.sendMessage(ChatColor.GREEN + "Teams refreshed. " +
				EventsPlugin.get().getTeamRegistry().getTeams().size() + " teams loaded.");
	}
}
