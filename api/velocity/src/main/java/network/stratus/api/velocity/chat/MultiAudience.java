/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.chat;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.proxy.Player;

import network.stratus.api.chat.Audience;
import network.stratus.api.i18n.Translation;
import network.stratus.api.velocity.StratusAPIVelocity;

/**
 * Represents an {@link Audience} of multiple targets.
 * 
 * @author Ian Ballingall
 *
 */
public class MultiAudience implements Audience {

	private Collection<? extends Audience> targets;

	public MultiAudience(Collection<? extends Audience> targets) {
		this.targets = targets;
	}

	@Override
	public void sendMessage(Translation translation) {
		for (Audience target : targets) {
			target.sendMessage(translation);
		}
	}

	@Override
	public void sendMessage(String stringKey) {
		for (Audience target : targets) {
			target.sendMessage(stringKey);
		}
	}

	@Override
	public void sendMessage(String stringKey, Object... args) {
		for (Audience target : targets) {
			target.sendMessage(stringKey, args);
		}
	}

	@Override
	public void sendMessageRaw(String message) {
		for (Audience target : targets) {
			target.sendMessageRaw(message);
		}
	}

	/**
	 * Builds a new {@link Audience} of multiple targets, based on the conditions
	 * they satisfy. Targets can be included or excluded by satisfying
	 * {@link Predicate}s. Convenience methods for permission nodes are included.
	 */
	public static class Builder {

		private Set<CommandSource> targets = new HashSet<>();
		private List<Predicate<CommandSource>> includePredicates = new LinkedList<>();
		private List<Predicate<CommandSource>> excludePredicates = new LinkedList<>();

		/**
		 * Add the specified target to the {@link MultiAudience}.
		 */
		public Builder addTarget(CommandSource target) {
			targets.add(target);
			return this;
		}

		/**
		 * Add the specified targets to the {@link MultiAudience}.
		 */
		public Builder addTargets(Collection<? extends CommandSource> targets) {
			this.targets.addAll(targets);
			return this;
		}

		/**
		 * Add all {@link Player}s to the {@link MultiAudience}.
		 */
		public Builder global() {
			targets.addAll(StratusAPIVelocity.get().getServer().getAllPlayers());
			return this;
		}

		/**
		 * Add the console to the {@link MultiAudience}.
		 */
		public Builder console() {
			targets.add(StratusAPIVelocity.get().getServer().getConsoleCommandSource());
			return this;
		}

		/**
		 * Add all {@link Player}s who have the given permission node.
		 */
		public Builder includePermission(String permission) {
			includePredicates.add(sender -> sender.hasPermission(permission));
			return this;
		}

		/**
		 * Add all {@link Player}s who have the given permission nodes.
		 */
		public Builder includePermissions(Collection<String> permissions) {
			permissions.forEach(this::includePermission);
			return this;
		}

		/**
		 * Exclude {@link Player}s who have the given permission node.
		 */
		public Builder excludePermission(String permission) {
			excludePredicates.add(sender -> sender.hasPermission(permission));
			return this;
		}

		/**
		 * Exclude {@link Player}s who have the given permission nodes.
		 */
		public Builder excludePermissions(Collection<String> permissions) {
			permissions.forEach(this::excludePermission);
			return this;
		}

		/**
		 * Include {@link Player}s who satisfy the given {@link Predicate}.
		 */
		public Builder includePredicate(Predicate<CommandSource> predicate) {
			includePredicates.add(predicate);
			return this;
		}

		/**
		 * Include {@link Player}s who satisfy the given {@link Predicate}s.
		 */
		public Builder includePredicates(Collection<Predicate<CommandSource>> predicates) {
			includePredicates.addAll(predicates);
			return this;
		}

		/**
		 * Exclude {@link Player}s who satisfy the given {@link Predicate}.
		 */
		public Builder excludePredicate(Predicate<CommandSource> predicate) {
			excludePredicates.add(predicate);
			return this;
		}

		/**
		 * Exclude {@link Player}s who satisfy the given {@link Predicate}s.
		 */
		public Builder excludePredicates(Collection<Predicate<CommandSource>> predicates) {
			excludePredicates.addAll(predicates);
			return this;
		}

		/**
		 * Construct the corresponding {@link MultiAudience}. Note that firstly,
		 * {@link Player}s who satisfy the {@code includePredicates} will be added.
		 * Then, of those targets, those satisfying {@code exlucdePredicates} will be
		 * removed.
		 * 
		 * @return The {@link MultiAudience}
		 */
		public MultiAudience build() {
			for (Player player : StratusAPIVelocity.get().getServer().getAllPlayers()) {
				for (Predicate<CommandSource> predicate : includePredicates) {
					if (predicate.test(player)) {
						targets.add(player);
						break;
					}
				}
			}

			targets = targets.stream().filter(target -> {
				for (Predicate<CommandSource> predicate : excludePredicates) {
					if (predicate.test(target)) {
						return false;
					}
				}

				return true;
			}).collect(Collectors.toSet());

			return new MultiAudience(targets.stream().map(SingleAudience::new).collect(Collectors.toSet()));
		}

	}

}
