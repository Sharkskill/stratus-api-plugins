/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.listeners;

import java.net.InetSocketAddress;
import java.util.Date;

import com.velocitypowered.api.event.PostOrder;
import com.velocitypowered.api.event.ResultedEvent.ComponentResult;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.connection.LoginEvent;
import com.velocitypowered.api.proxy.Player;

import net.kyori.adventure.text.Component;
import network.stratus.api.client.RequestFailureException;
import network.stratus.api.i18n.Translator;
import network.stratus.api.models.punishment.Punishment;
import network.stratus.api.velocity.StratusAPIVelocity;
import network.stratus.api.velocity.chat.SingleAudience;
import network.stratus.api.velocity.requests.LoginRequest;
import network.stratus.api.requests.RegisterRequest;
import network.stratus.api.velocity.responses.LoginResponse;

import javax.annotation.Nullable;

/**
 * Listens on relevant login events and prohibits banned players and IPs from
 * joining.
 *
 * @author Ian Ballingall
 */
public class LoginListener {

	@Nullable
	private final String registrationHost;

	public LoginListener(@Nullable String registrationHost) {
		this.registrationHost = registrationHost;
	}

	@Subscribe
	public void onLogin(LoginEvent event) {
		if (!event.getResult().isAllowed())
			return;

		Translator translator = StratusAPIVelocity.get().getTranslator();
		Player player = event.getPlayer();
		SingleAudience audience = new SingleAudience(player);
		LoginResponse response = login(player);

		if (response.getPunishment() != null) {
			Punishment punishment = response.getPunishment().getObject();
			final String message;
			if (punishment.getExpiry() == null) {
				message = audience.translate("punishment.ban.permanent.message", punishment.getReason());
			} else {
				message = audience.translate("punishment.ban.temporary.message",
						punishment.getReason(),
						translator.getTimeDifferenceString(player.getPlayerSettings().getLocale(),
								punishment.getExpiry(),
								new Date()));
			}

			event.setResult(ComponentResult.denied(Component.text(message)));
		} else if (response.isIpBanned()) {
			String message = audience.translate("punishment.ipban.message");
			event.setResult(ComponentResult.denied(Component.text(message)));
		}
	}

	@Subscribe(order = PostOrder.FIRST)
	public void onRegister(LoginEvent event) {
		if (!event.getResult().isAllowed() || registrationHost == null)
			return;

		Player player = event.getPlayer();

		// Check the player's virtual host to see if they're registering
		if (!player.getVirtualHost().isPresent())
			return;

		InetSocketAddress virtualHost = player.getVirtualHost().get();
		int separator = virtualHost.getHostString().indexOf('.');
		if (separator <= 0)
			return;

		String domain = virtualHost.getHostString().substring(separator + 1);
		if (!domain.equals(registrationHost))
			return;

		String serverId = virtualHost.getHostString().substring(0, separator);
		SingleAudience audience = new SingleAudience(player);
		try {
			login(player); // Make login request for logging purposes and to ensure the player exists

			RegisterRequest.Response response =
					new RegisterRequest(player.getUniqueId(), serverId).make(StratusAPIVelocity.get().getApiClient());

			String message = audience.translate("registration.code", response.getCode());
			event.setResult(ComponentResult.denied(Component.text(message)));
		} catch (RequestFailureException e) {
			e.printStackTrace();
			StratusAPIVelocity.get()
					.getLogger()
					.error("Failed to create registration code for " + player.getUsername());
			e.getResponse().ifPresent(error -> StratusAPIVelocity.get().getLogger().error(error.getDescription()));

			String message = audience.translate("registration.failure");
			event.setResult(ComponentResult.denied(Component.text(message)));
		}
	}

	private LoginResponse login(Player player) {
		return new LoginRequest(player.getUniqueId(),
				player.getUsername(),
				player.getRemoteAddress().getAddress().getHostAddress()
		).make(StratusAPIVelocity.get().getApiClient());
	}

}
