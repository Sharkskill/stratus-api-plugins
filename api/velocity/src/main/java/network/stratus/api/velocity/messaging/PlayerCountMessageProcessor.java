/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.messaging;

import network.stratus.api.messaging.MessageProcessor;
import network.stratus.api.velocity.StratusAPIVelocity;
import network.stratus.api.velocity.proxy.PlayerCountManager;

/**
 * Processes a player count message received from other proxies.
 * 
 * @author Ian Ballingall
 *
 */
public class PlayerCountMessageProcessor implements MessageProcessor<PlayerCount> {

	private String proxyName;

	public PlayerCountMessageProcessor(String proxyName) {
		this.proxyName = proxyName;
	}

	@Override
	public void process(PlayerCount object) {
		if (!object.getProxy().equals(proxyName)) {
			PlayerCountManager manager = StratusAPIVelocity.get().getPlayerCountManager();
			if (object.shouldDestroy()) {
				manager.clearPlayerCount(object.getProxy());
			} else {
				manager.setPlayerCount(object.getProxy(), object.getCount());
			}
		}
	}

}
